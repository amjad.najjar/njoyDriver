/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';

class $AssetsAnimationsGen {
  const $AssetsAnimationsGen();

  /// File path: assets/animations/car_animation.json
  LottieGenImage get carAnimation =>
      const LottieGenImage('assets/animations/car_animation.json');

  /// File path: assets/animations/loading.json
  LottieGenImage get loading =>
      const LottieGenImage('assets/animations/loading.json');

  /// File path: assets/animations/no_internet.json
  LottieGenImage get noInternet =>
      const LottieGenImage('assets/animations/no_internet.json');

  /// File path: assets/animations/nodata.json
  LottieGenImage get nodata =>
      const LottieGenImage('assets/animations/nodata.json');

  /// File path: assets/animations/success.json
  LottieGenImage get success =>
      const LottieGenImage('assets/animations/success.json');

  /// List of all assets
  List<LottieGenImage> get values =>
      [carAnimation, loading, noInternet, nodata, success];
}

class $AssetsFontsGen {
  const $AssetsFontsGen();

  /// File path: assets/fonts/ExpoArabic-Bold.ttf
  String get expoArabicBold => 'assets/fonts/ExpoArabic-Bold.ttf';

  /// File path: assets/fonts/ExpoArabic-Book.ttf
  String get expoArabicBook => 'assets/fonts/ExpoArabic-Book.ttf';

  /// File path: assets/fonts/ExpoArabic-Light.ttf
  String get expoArabicLight => 'assets/fonts/ExpoArabic-Light.ttf';

  /// File path: assets/fonts/ExpoArabic-Medium.ttf
  String get expoArabicMedium => 'assets/fonts/ExpoArabic-Medium.ttf';

  /// File path: assets/fonts/ExpoArabic-SemiBold.ttf
  String get expoArabicSemiBold => 'assets/fonts/ExpoArabic-SemiBold.ttf';

  /// List of all assets
  List<String> get values => [
        expoArabicBold,
        expoArabicBook,
        expoArabicLight,
        expoArabicMedium,
        expoArabicSemiBold
      ];
}

class $AssetsIconsGen {
  const $AssetsIconsGen();

  /// File path: assets/icons/Mask Group 5.svg
  SvgGenImage get maskGroup5 =>
      const SvgGenImage('assets/icons/Mask Group 5.svg');

  /// File path: assets/icons/about_us.svg
  SvgGenImage get aboutUs => const SvgGenImage('assets/icons/about_us.svg');

  /// File path: assets/icons/account.svg
  SvgGenImage get account => const SvgGenImage('assets/icons/account.svg');

  /// File path: assets/icons/airport.svg
  SvgGenImage get airport => const SvgGenImage('assets/icons/airport.svg');

  /// File path: assets/icons/arrow-back.svg
  SvgGenImage get arrowBack => const SvgGenImage('assets/icons/arrow-back.svg');

  /// File path: assets/icons/arrow.svg
  SvgGenImage get arrow => const SvgGenImage('assets/icons/arrow.svg');

  /// File path: assets/icons/breif_case.svg
  SvgGenImage get breifCase => const SvgGenImage('assets/icons/breif_case.svg');

  /// File path: assets/icons/call_center.svg
  SvgGenImage get callCenter =>
      const SvgGenImage('assets/icons/call_center.svg');

  /// File path: assets/icons/camera.svg
  SvgGenImage get camera => const SvgGenImage('assets/icons/camera.svg');

  /// File path: assets/icons/clock.svg
  SvgGenImage get clock => const SvgGenImage('assets/icons/clock.svg');

  /// File path: assets/icons/closed eye.svg
  SvgGenImage get closedEye => const SvgGenImage('assets/icons/closed eye.svg');

  /// File path: assets/icons/conditions.svg
  SvgGenImage get conditions =>
      const SvgGenImage('assets/icons/conditions.svg');

  /// File path: assets/icons/duration.svg
  SvgGenImage get duration => const SvgGenImage('assets/icons/duration.svg');

  /// File path: assets/icons/edit.svg
  SvgGenImage get edit => const SvgGenImage('assets/icons/edit.svg');

  /// File path: assets/icons/filter.svg
  SvgGenImage get filter => const SvgGenImage('assets/icons/filter.svg');

  /// File path: assets/icons/from.svg
  SvgGenImage get from => const SvgGenImage('assets/icons/from.svg');

  /// File path: assets/icons/gem.svg
  SvgGenImage get gem => const SvgGenImage('assets/icons/gem.svg');

  /// File path: assets/icons/gps.svg
  SvgGenImage get gps => const SvgGenImage('assets/icons/gps.svg');

  /// File path: assets/icons/guest_user.svg
  SvgGenImage get guestUser => const SvgGenImage('assets/icons/guest_user.svg');

  /// File path: assets/icons/help.svg
  SvgGenImage get help => const SvgGenImage('assets/icons/help.svg');

  /// File path: assets/icons/history.svg
  SvgGenImage get history => const SvgGenImage('assets/icons/history.svg');

  /// File path: assets/icons/home.svg
  SvgGenImage get home => const SvgGenImage('assets/icons/home.svg');

  /// File path: assets/icons/location.svg
  SvgGenImage get location => const SvgGenImage('assets/icons/location.svg');

  /// File path: assets/icons/logo.svg
  SvgGenImage get logo => const SvgGenImage('assets/icons/logo.svg');

  /// File path: assets/icons/logout.svg
  SvgGenImage get logout => const SvgGenImage('assets/icons/logout.svg');

  /// File path: assets/icons/luggage.svg
  SvgGenImage get luggage => const SvgGenImage('assets/icons/luggage.svg');

  /// File path: assets/icons/menu.svg
  SvgGenImage get menu => const SvgGenImage('assets/icons/menu.svg');

  /// File path: assets/icons/money.svg
  SvgGenImage get money => const SvgGenImage('assets/icons/money.svg');

  /// File path: assets/icons/notification.svg
  SvgGenImage get notification =>
      const SvgGenImage('assets/icons/notification.svg');

  /// File path: assets/icons/open eye.svg
  SvgGenImage get openEye => const SvgGenImage('assets/icons/open eye.svg');

  /// File path: assets/icons/plane.svg
  SvgGenImage get plane => const SvgGenImage('assets/icons/plane.svg');

  /// File path: assets/icons/privacy.svg
  SvgGenImage get privacy => const SvgGenImage('assets/icons/privacy.svg');

  /// File path: assets/icons/question.svg
  SvgGenImage get question => const SvgGenImage('assets/icons/question.svg');

  /// File path: assets/icons/right.svg
  SvgGenImage get right => const SvgGenImage('assets/icons/right.svg');

  /// File path: assets/icons/routing.svg
  SvgGenImage get routing => const SvgGenImage('assets/icons/routing.svg');

  /// File path: assets/icons/search.svg
  SvgGenImage get search => const SvgGenImage('assets/icons/search.svg');

  /// File path: assets/icons/suitcase.svg
  SvgGenImage get suitcase => const SvgGenImage('assets/icons/suitcase.svg');

  /// File path: assets/icons/syria_flag.svg
  SvgGenImage get syriaFlag => const SvgGenImage('assets/icons/syria_flag.svg');

  /// File path: assets/icons/taxi.svg
  SvgGenImage get taxi => const SvgGenImage('assets/icons/taxi.svg');

  /// File path: assets/icons/time.svg
  SvgGenImage get time => const SvgGenImage('assets/icons/time.svg');

  /// File path: assets/icons/to.svg
  SvgGenImage get to => const SvgGenImage('assets/icons/to.svg');

  /// File path: assets/icons/tracking.svg
  SvgGenImage get tracking => const SvgGenImage('assets/icons/tracking.svg');

  /// File path: assets/icons/translation.svg
  SvgGenImage get translation =>
      const SvgGenImage('assets/icons/translation.svg');

  /// File path: assets/icons/warning.svg
  SvgGenImage get warning => const SvgGenImage('assets/icons/warning.svg');

  /// File path: assets/icons/way.svg
  SvgGenImage get way => const SvgGenImage('assets/icons/way.svg');

  /// List of all assets
  List<SvgGenImage> get values => [
        maskGroup5,
        aboutUs,
        account,
        airport,
        arrowBack,
        arrow,
        breifCase,
        callCenter,
        camera,
        clock,
        closedEye,
        conditions,
        duration,
        edit,
        filter,
        from,
        gem,
        gps,
        guestUser,
        help,
        history,
        home,
        location,
        logo,
        logout,
        luggage,
        menu,
        money,
        notification,
        openEye,
        plane,
        privacy,
        question,
        right,
        routing,
        search,
        suitcase,
        syriaFlag,
        taxi,
        time,
        to,
        tracking,
        translation,
        warning,
        way
      ];
}

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/delivery_location.png
  AssetGenImage get deliveryLocation =>
      const AssetGenImage('assets/images/delivery_location.png');

  /// File path: assets/images/driverMarker.png
  AssetGenImage get driverMarker =>
      const AssetGenImage('assets/images/driverMarker.png');

  /// File path: assets/images/from.png
  AssetGenImage get from => const AssetGenImage('assets/images/from.png');

  /// File path: assets/images/home_location.png
  AssetGenImage get homeLocation =>
      const AssetGenImage('assets/images/home_location.png');

  /// File path: assets/images/logo.png
  AssetGenImage get logo => const AssetGenImage('assets/images/logo.png');

  /// File path: assets/images/splash.png
  AssetGenImage get splash => const AssetGenImage('assets/images/splash.png');

  /// File path: assets/images/store_location.png
  AssetGenImage get storeLocation =>
      const AssetGenImage('assets/images/store_location.png');

  /// File path: assets/images/to.png
  AssetGenImage get to => const AssetGenImage('assets/images/to.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        deliveryLocation,
        driverMarker,
        from,
        homeLocation,
        logo,
        splash,
        storeLocation,
        to
      ];
}

class Assets {
  Assets._();

  static const $AssetsAnimationsGen animations = $AssetsAnimationsGen();
  static const $AssetsFontsGen fonts = $AssetsFontsGen();
  static const $AssetsIconsGen icons = $AssetsIconsGen();
  static const $AssetsImagesGen images = $AssetsImagesGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(this._assetName);

  final String _assetName;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme theme = const SvgTheme(),
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    return SvgPicture.asset(
      _assetName,
      key: key,
      matchTextDirection: matchTextDirection,
      bundle: bundle,
      package: package,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      theme: theme,
      colorFilter: colorFilter,
      color: color,
      colorBlendMode: colorBlendMode,
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class LottieGenImage {
  const LottieGenImage(this._assetName);

  final String _assetName;

  LottieBuilder lottie({
    Animation<double>? controller,
    bool? animate,
    FrameRate? frameRate,
    bool? repeat,
    bool? reverse,
    LottieDelegates? delegates,
    LottieOptions? options,
    void Function(LottieComposition)? onLoaded,
    LottieImageProviderFactory? imageProviderFactory,
    Key? key,
    AssetBundle? bundle,
    Widget Function(BuildContext, Widget, LottieComposition?)? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    double? width,
    double? height,
    BoxFit? fit,
    AlignmentGeometry? alignment,
    String? package,
    bool? addRepaintBoundary,
    FilterQuality? filterQuality,
    void Function(String)? onWarning,
  }) {
    return Lottie.asset(
      _assetName,
      controller: controller,
      animate: animate,
      frameRate: frameRate,
      repeat: repeat,
      reverse: reverse,
      delegates: delegates,
      options: options,
      onLoaded: onLoaded,
      imageProviderFactory: imageProviderFactory,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      package: package,
      addRepaintBoundary: addRepaintBoundary,
      filterQuality: filterQuality,
      onWarning: onWarning,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
