class SendUser {
  final String phone;
  final String fcmToken;
  final String? code;
  SendUser({required this.phone, this.code, required this.fcmToken});
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {
      'mobile': phone,
      "verification_code": code,
      'notification_token': fcmToken
    };
    json.removeWhere((key, value) => value == null);
    return json;
  }
}
