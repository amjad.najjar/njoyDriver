class Reasons {
  final int id;
  final String name;

  const Reasons({required this.id, required this.name});

  factory Reasons.fromJson(Map<String, dynamic> json) =>
      Reasons(id: json['id'], name: json['name']);
}
