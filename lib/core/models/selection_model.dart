class AppSelectionModel {
  int id;
  String? title;
  String? icon;
  Function? callBack;
  AppSelectionModel({required this.id, this.title, this.icon, this.callBack});

  factory AppSelectionModel.fromJson(Map<String, dynamic> json) =>
      AppSelectionModel(id: json['id'], title: json['name']);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppSelectionModel &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          title == other.title;

  @override
  int get hashCode => id.hashCode;
}
