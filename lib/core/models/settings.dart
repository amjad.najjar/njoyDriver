class Setting {
  final String value;
  Setting(this.value);

  factory Setting.fromJson(Map<String, dynamic> json) => Setting(json["value"]);

  num get asNum => num.parse(value);
  double get asDouble => double.parse(value);
}
