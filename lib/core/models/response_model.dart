import 'dart:convert';

class ResponseModel {
  dynamic code;
  dynamic data;
  String? message;
  String? statusCode;
  bool success;
  Map<String, List<dynamic>>? errors;

  ResponseModel(
      {this.code,
      this.data,
      this.message,
      this.statusCode,
      this.errors,
      this.success = true});

  factory ResponseModel.fromJson(Map<String, dynamic> json) => ResponseModel(
        code: json['code'],
        data: json['data'],
        errors: json['errors'] != null
            ? Map<String, List<dynamic>>.from(json['errors'])
            : null,
        message: json['message'],
      );
}
