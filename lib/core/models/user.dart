import 'dart:developer';

import 'package:hive_flutter/hive_flutter.dart';

import 'enums/role.dart';

part 'user.g.dart';

@HiveType(typeId: 1)
class User {
  @HiveField(0)
  int? id;
  @HiveField(1)
  String? phone;
  @HiveField(2)
  String? email;
  @HiveField(3)
  String? name;
  @HiveField(4)
  Role? role;
  @HiveField(5)
  String? fcmToken;
  @HiveField(6)
  String? img;
  @HiveField(7)
  bool? isOnline;

  User(
      {this.id,
      this.phone,
      this.name,
      this.role,
      this.email,
      this.img,
      this.isOnline,
      this.fcmToken});
  Map<String, dynamic> toJson() => {
        'mobile': phone,
        'notification_token': fcmToken,
        'name': name,
        'email': email,
        "image": img
      };
  factory User.fromJson(Map<String, dynamic> json) {
    log(json.toString());
    return User(
        id: json['id'],
        phone: json['mobile'],
        name: json['name'],
        email: json['email'],
        img: json['image'],
        fcmToken: json['notification_token']);
  }
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is User && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}
