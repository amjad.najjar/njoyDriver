import 'package:njoy_driver/core/models/abstract_city.dart';

import 'enums/trip_state.dart';
import 'enums/trip_type.dart';
import 'location_model.dart';

class Trip {
  final int id;
  final TripType type;
  final TripState state;
  final DateTime createdAt;
  final dynamic content;
  final num? price;
  final num? duration;
  final num? distance;

  const Trip(
      {required this.id,
      required this.content,
      required this.createdAt,
      required this.state,
      required this.type,
      this.price,
      this.distance,
      this.duration});
  factory Trip.fromJson(Map<String, dynamic> json) {
    TripType type = TripType.fromString(json['type']);

    return Trip(
        id: json['id'],
        content: type.parse(json),
        price: json['price'],
        distance: json['distance'],
        duration: json['duration'],
        createdAt: DateTime.parse(json['created_at']),
        state: TripState.fromJson(json['trip_status']),
        type: type);
  }
  dynamic get _content {
    switch (type) {
      case TripType.taxi:
        return content as TaxiTrip;
      case TripType.airport:
        return content as AirpotTrip;
      case TripType.travel:
        return content as TravelTrip;
      case TripType.flex:
        return content as FlexTrip;
    }
  }

  String? get distName {
    if (content is TaxiTrip) return _content.dist.address;
    if (content is TravelTrip) return _content.to.name;
    if (content is AirpotTrip) return _content.airport.name;
    return null;
  }
}

abstract class TripContent {
  String get srcName;
}

class ShortTripContent extends TripContent {
  final num? deluxe;
  final num? tracking;
  final LocationModel src;
  ShortTripContent(
      {required this.deluxe, required this.tracking, required this.src});
  factory ShortTripContent.fromJson(Map<String, dynamic> json) =>
      ShortTripContent(
          deluxe: json['deluxe'],
          tracking: json['tracking'],
          src: LocationModel.fromJson(json['source']));

  @override
  String get srcName => src.address;
}

class TaxiTrip extends ShortTripContent {
  final LocationModel dist;
  TaxiTrip(
      {required this.dist,
      required super.deluxe,
      required super.tracking,
      required super.src});

  @override
  factory TaxiTrip.fromJson(Map<String, dynamic> json) {
    ShortTripContent content = ShortTripContent.fromJson(json);
    return TaxiTrip(
        dist: LocationModel.fromJson(json['destination']),
        deluxe: content.deluxe,
        tracking: content.tracking,
        src: content.src);
  }
}

class FlexTrip extends ShortTripContent {
  FlexTrip(
      {required super.deluxe, required super.tracking, required super.src});
  factory FlexTrip.fromJson(Map<String, dynamic> json) {
    ShortTripContent content = ShortTripContent.fromJson(json);
    return FlexTrip(
        deluxe: content.deluxe, tracking: content.tracking, src: content.src);
  }
}

class TravelTrip extends TripContent {
  final City from;
  final City to;
  TravelTrip({required this.from, required this.to});
  factory TravelTrip.fromJson(Map<String, dynamic> json) => TravelTrip(
      from: City.fromJson(json['from_city']),
      to: City.fromJson(json['to_city']));

  @override
  String get srcName => from.name;
}

class AirpotTrip extends TripContent {
  final Region fromRegion;
  final AirPort airport;
  AirpotTrip({required this.fromRegion, required this.airport});
  factory AirpotTrip.fromJson(Map<String, dynamic> json) => AirpotTrip(
      airport: AirPort.fromJson(json['to_airport']),
      fromRegion: Region.fromJson(json['from_region']));
  @override
  String get srcName => fromRegion.name;
}
