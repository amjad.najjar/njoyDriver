import 'package:get/get.dart';
import 'package:njoy_driver/core/models/trip.dart';
import 'package:njoy_driver/gen/assets.gen.dart';

enum TripType {
  flex,
  taxi,
  travel,
  airport;

  factory TripType.fromString(String val) {
    switch (val) {
      case 'flex':
        return flex;
      case 'taxi':
        return taxi;
      case 'travel':
        return travel;
      case 'airport':
        return airport;
      default:
        return taxi;
    }
  }
  TripContent parse(Map<String, dynamic> json) {
    switch (this) {
      case taxi:
        return TaxiTrip.fromJson(json);
      case airport:
        return AirpotTrip.fromJson(json);
      case travel:
        return TravelTrip.fromJson(json);
      case flex:
        return FlexTrip.fromJson(json);
    }
  }

  String get name {
    switch (this) {
      case taxi:
        return 'Taxi'.tr;
      case airport:
        return 'Airport'.tr;
      case travel:
        return 'Travel'.tr;
      case flex:
        return 'Flex'.tr;
    }
  }

  String get icon {
    switch (this) {
      case taxi:
        return Assets.icons.taxi.path;
      case airport:
        return Assets.icons.plane.path;
      case travel:
        return Assets.icons.luggage.path;
      case flex:
        return Assets.icons.way.path;
    }
  }
}
