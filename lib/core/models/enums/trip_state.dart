import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/styles/colors.dart';

enum TripState {
  pendding,
  onGoing,
  accepted,
  canceled,
  completed;

  factory TripState.fromJson(Map<String, dynamic> json) {
    int state = json['id'];
    switch (state) {
      case 1:
        return pendding;
      case 2:
        return onGoing;
      case 3:
        return accepted;
      case 4:
        return completed;
      case 5:
        return canceled;
      default:
        return pendding;
    }
  }
  @override
  String toString() {
    switch (this) {
      case accepted:
        return "accepted".tr;
      case canceled:
        return "canceled".tr;
      case completed:
        return "completed".tr;
      case onGoing:
        return "ongoing".tr;
      case pendding:
        return "pendding".tr;
    }
  }

  Color? get color {
    switch (this) {
      case TripState.pendding:
        return AppColors.pending;
      case TripState.completed:
        return AppColors.done;
      case TripState.onGoing:
        return AppColors.secondary;
      case TripState.accepted:
        return AppColors.primary;
      case TripState.canceled:
        return AppColors.refused;
      default:
        return null;
    }
  }
}
