// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'role.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class RoleAdapter extends TypeAdapter<Role> {
  @override
  final int typeId = 2;

  @override
  Role read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return Role.unregisteredUser;
      case 1:
        return Role.newUser;
      case 2:
        return Role.user;
      case 3:
        return Role.guest;
      default:
        return Role.unregisteredUser;
    }
  }

  @override
  void write(BinaryWriter writer, Role obj) {
    switch (obj) {
      case Role.unregisteredUser:
        writer.writeByte(0);
        break;
      case Role.newUser:
        writer.writeByte(1);
        break;
      case Role.user:
        writer.writeByte(2);
        break;
      case Role.guest:
        writer.writeByte(3);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RoleAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
