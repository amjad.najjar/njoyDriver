enum NotificationType {
  normal;

  static fromString(String? key) {
    switch (key) {
      case "NORMAL":
        return normal;
    }
  }

  @override
  String toString() {
    switch (this) {
      case normal:
        return "NORMAL";
    }
  }
}
