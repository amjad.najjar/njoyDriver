import 'package:hive/hive.dart';
part 'role.g.dart';

@HiveType(typeId: 2)
enum Role {
  @HiveField(0)
  unregisteredUser,
  @HiveField(1)
  newUser,
  @HiveField(2)
  user,
  @HiveField(3)
  guest;

  bool get isAuthenticated => this == user;
}
