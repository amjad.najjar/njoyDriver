import 'package:google_maps_flutter/google_maps_flutter.dart';

class LocationModel {
  final LatLng point;
  final String address;
  const LocationModel({required this.address, required this.point});
  factory LocationModel.fromJson(Map<String, dynamic> json) => LocationModel(
      address: json['address'], point: LatLng(json['lat'], json['lng']));
}
