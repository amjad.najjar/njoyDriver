class Slider {
  final int id;
  final String image;
  final String? link;

  const Slider({required this.id, required this.image, this.link});

  factory Slider.fromJson(Map<String, dynamic> json) =>
      Slider(id: json['id'], image: json['image'], link: json['link']);
}
