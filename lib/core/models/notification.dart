import 'dart:developer';

enum NotificationType { order, public }

class NotificationModel {
  String? title;
  String? body;
  String? createdAt;

  NotificationModel({
    this.title,
    this.body,
    this.createdAt,
  });

  NotificationModel.fromJson(Map<String, dynamic> json) {
    log(json.toString());
    dynamic myJson = json['notification'] ?? json;
    title = myJson['title'];
    body = myJson['text'];
    createdAt = myJson['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['text'] = this.body;
    data['created_at'] = this.createdAt;
    return data;
  }
}
