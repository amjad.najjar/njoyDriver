abstract class Place {
  final int id;
  final String name;
  Place({required this.id, required this.name});
}

class City extends Place {
  City({required super.id, required super.name});

  factory City.fromJson(Map<String, dynamic> json) =>
      City(id: json['id'], name: json['name']);
}

class AirPort extends Place {
  AirPort({required super.id, required super.name});

  factory AirPort.fromJson(Map<String, dynamic> json) =>
      AirPort(id: json['id'], name: json['name']);
}

class Region extends Place {
  final City? city;

  Region({required super.id, required super.name, this.city});

  factory Region.fromJson(Map<String, dynamic> json) => Region(
      id: json['id'],
      name: json['name'],
      city: json['city'] != null ? City.fromJson(json['city']) : null);
}
