class TripInfo {
  final num price;
  final String src;
  final String dist;
  final num distance;
  final num duration;
  TripInfo(
      {required this.price,
      required this.src,
      required this.dist,
      required this.distance,
      required this.duration});

  factory TripInfo.fromJson(Map<String, dynamic> json) => TripInfo(
      price: json['price'],
      src: json['source_addresses'][0],
      dist: json['destination_addresses'][0],
      distance: json['distance']['value'],
      duration: json['duration']['value']);
}
