import 'package:njoy_driver/core/models/user.dart';

class AuthUser {
  final String token;
  final User user;
  AuthUser({required this.token, required this.user});
  factory AuthUser.fromJson(Map<String, dynamic> json) =>
      AuthUser(token: json['token'], user: User.fromJson(json['driver']));
}
