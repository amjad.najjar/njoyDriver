import '../models/response_model.dart';

class ParsingError implements Exception {
  ResponseModel? response;
  ParsingError({this.response});
}
