import 'package:njoy_driver/core/models/response_model.dart';

class AppException implements Exception {
  final ExceptionType type;
  final int? statusCode;
  final ResponseModel? response;

  AppException({required this.type, this.statusCode, this.response});
}

enum ExceptionType {
  flowError,
  serverError,
  connectionError,
  validationError,
  tooManyRequest,
  unknown,
  unauthenticated,
  parsingError;

  String get name {
    switch (this) {
      case flowError:
        return "Flow Error";

      case unknown:
        return "unknown Error";

      case parsingError:
        return "Parsing Error";

      case tooManyRequest:
        return "Too Many Request";

      case validationError:
        return "Validation Error";

      case connectionError:
        return "Connection Error";

      case serverError:
        return "Server Error";
      case unauthenticated:
        return "Unauthenticated";
    }
  }
}
