enum ErrorType {
  validationError,
  flowError,
  serverError,
  notAuthenticated,
  noConnection,
  tooManyAttempts,
  parsingError;

  static ErrorType? fromStatusCode(int statusCode) {
    switch (statusCode) {
      case 401:
        return notAuthenticated;
      case 500:
        return serverError;
      case 422:
        return validationError;
      case 429:
        return tooManyAttempts;
      default:
        return null;
    }
  }
}
