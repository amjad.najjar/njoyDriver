import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/gen/assets.gen.dart';
import 'controller.dart';
import 'options.dart';

// ignore: must_be_immutable
class Pagination<T> extends StatelessWidget {
  final String tag;
  final PaginationController<T> controller;
  final PaginationOptions option;
  final bool shrinkWrap;
  final EdgeInsetsGeometry padding;
  final Axis scrollDirection;
  final ScrollPhysics physics;
  final SliverGridDelegate? gridDelegate;
  final bool refresh;
  final bool refreshOnrebuild;
  final bool reverse;
  Widget? loading;
  final Widget initialLoading;
  final Widget Function(int, T? item) itemBuilder;

  Pagination({
    required this.tag,
    required this.controller,
    required this.itemBuilder,
    this.option = PaginationOptions.ListView,
    this.shrinkWrap = false,
    this.padding = EdgeInsets.zero,
    this.scrollDirection = Axis.vertical,
    this.physics = const AlwaysScrollableScrollPhysics(),
    this.gridDelegate,
    this.refresh = false,
    this.refreshOnrebuild = false,
    this.loading,
    this.initialLoading =
        const Center(child: CircularProgressIndicator() // const Loading(),
            ),
    this.reverse = false,
  }) {
    if (option == PaginationOptions.GridView) {
      assert(gridDelegate != null);
    }
    if (loading == null) {
      loading = initialLoading;
      // loading = Row(
      //   mainAxisAlignment: MainAxisAlignment.center,
      //   children: [Text('loading'.tr), SizedBox(width: 12.0), Loading()],
      // );
    }
  }
  @override
  Widget build(BuildContext context) {
    PaginationController<T> paginationController;
    try {
      paginationController = Get.find(tag: tag);
      if (refreshOnrebuild) {
        paginationController.refreshData();
      }
    } catch (e) {
      paginationController = Get.put(controller, tag: tag);
    }
    return Obx(() {
      if (paginationController.data.hasError &&
          paginationController.data.valueLength == 0) {
        return RefreshIndicator(
            onRefresh: () => paginationController.refreshData(),
            child: Center(child: Assets.animations.nodata.lottie()));
      } else if (paginationController.data.loading) {
        return initialLoading;
      }
      switch (option) {
        case PaginationOptions.ListView:
          return Obx(
            () => refresh
                ? RefreshIndicator(
                    onRefresh: () => paginationController.refreshData(),
                    child: ListView.builder(
                      shrinkWrap: shrinkWrap,
                      physics: physics,
                      padding: padding,
                      reverse: reverse,
                      scrollDirection: scrollDirection,
                      controller: physics is NeverScrollableScrollPhysics
                          ? null
                          : paginationController.scrollController,
                      itemCount: paginationController.data.valueLength! + 1,
                      itemBuilder: (context, index) {
                        if (index == paginationController.data.valueLength) {
                          return Obx(
                            () {
                              if (paginationController.isFinished) {
                                return SizedBox();
                              } else if (paginationController.loading) {
                                return loading!;
                              } else {
                                return Center(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text('some error'),
                                      SizedBox(width: 12.0),
                                      ElevatedButton(
                                        onPressed: () =>
                                            paginationController.loadData(),
                                        child: Text('retry'.tr),
                                      ),
                                    ],
                                  ),
                                );
                              }
                            },
                          );
                        }
                        return itemBuilder(
                          index,
                          paginationController.data.value![index]!,
                        );
                      },
                    ),
                  )
                : ListView.builder(
                    shrinkWrap: shrinkWrap,
                    physics: physics,
                    padding: padding,
                    reverse: reverse,
                    scrollDirection: scrollDirection,
                    controller: physics is NeverScrollableScrollPhysics
                        ? null
                        : paginationController.scrollController,
                    itemCount: (paginationController.data.valueLength ?? 0) + 1,
                    itemBuilder: (context, index) {
                      if (index == paginationController.data.valueLength) {
                        return Obx(
                          () {
                            if (paginationController.isFinished) {
                              return SizedBox();
                            } else if (paginationController.loading) {
                              return loading!;
                            } else {
                              return Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('some error'),
                                    SizedBox(width: 12.0),
                                    ElevatedButton(
                                      onPressed: () =>
                                          paginationController.loadData(),
                                      child: Text('retry'.tr),
                                    ),
                                  ],
                                ),
                              );
                            }
                          },
                        );
                      }
                      return itemBuilder(
                        index,
                        paginationController.data.value![index],
                      );
                    },
                  ),
          );
        case PaginationOptions.GridView:
          return Obx(
            () => refresh
                ? RefreshIndicator(
                    onRefresh: () => paginationController.refreshData(),
                    child: GridView.builder(
                      gridDelegate: gridDelegate!,
                      shrinkWrap: shrinkWrap,
                      physics: physics,
                      padding: padding,
                      reverse: reverse,
                      scrollDirection: scrollDirection,
                      controller: physics is NeverScrollableScrollPhysics
                          ? null
                          : paginationController.scrollController,
                      itemCount:
                          (paginationController.data.valueLength ?? 0) + 1,
                      itemBuilder: (context, index) {
                        if (index == paginationController.data.valueLength) {
                          return Obx(
                            () {
                              if (paginationController.isFinished) {
                                return SizedBox();
                              } else if (paginationController.loading) {
                                return loading!;
                              } else {
                                return Center(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text('some error'),
                                      SizedBox(width: 12.0),
                                      ElevatedButton(
                                        onPressed: () =>
                                            paginationController.loadData(),
                                        child: Text('retry'.tr),
                                      ),
                                    ],
                                  ),
                                );
                              }
                            },
                          );
                        }
                        return itemBuilder(
                          index,
                          paginationController.data.value![index],
                        );
                      },
                    ),
                  )
                : GridView.builder(
                    gridDelegate: gridDelegate!,
                    shrinkWrap: shrinkWrap,
                    physics: physics,
                    padding: padding,
                    reverse: reverse,
                    scrollDirection: scrollDirection,
                    controller: physics is NeverScrollableScrollPhysics
                        ? null
                        : paginationController.scrollController,
                    itemCount: (paginationController.data.valueLength ?? 0) + 1,
                    itemBuilder: (context, index) {
                      if (index == paginationController.data.valueLength) {
                        return Obx(
                          () {
                            if (paginationController.isFinished) {
                              return SizedBox();
                            } else if (paginationController.loading) {
                              return loading!;
                            } else {
                              return Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('some error'),
                                    SizedBox(width: 12.0),
                                    ElevatedButton(
                                      onPressed: () =>
                                          paginationController.loadData(),
                                      child: Text('retry'.tr),
                                    ),
                                  ],
                                ),
                              );
                            }
                          },
                        );
                      }
                      return itemBuilder(
                        index,
                        paginationController.data.value![index],
                      );
                    },
                  ),
          );
        default:
          return SizedBox.shrink();
      }
    });
  }
}
