import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'ar.dart';
import 'en.dart';

enum AppLocalization {
  ar,
  en;

  static AppLocalization fromString(String? val) => val == 'ar' ? ar : en;
  String get value => name;
  Locale get locale => Locale(value);

  String asString() {
    return name.tr;
  }
}

class Messages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {'ar': ar, 'en': en};
}
