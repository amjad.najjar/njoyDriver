import 'dart:convert';
import 'dart:developer';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/firebase_options.dart';

import '../constatnts/tags.dart';
import '../models/response_model.dart';
import '../repostries/data_base_repo.dart';

Future backgroundListener(RemoteMessage message) async {
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  Get.put(DataBaseRepo());
  NotificationService service = Get.put(NotificationService());
  await service.initNotifications(isBackground: true);
  log(message.data.toString());
  ResponseModel notificationModel = ResponseModel.fromJson(message.data);
  service.showNotification(
      title: notificationModel.data.title,
      summaryText: notificationModel.data.body,
      payload: jsonEncode(notificationModel.data),
      body: notificationModel.data.body);
}

class NotificationService extends GetxService {
  FirebaseMessaging? messageInstance;
  DataBaseRepo db = Get.find<DataBaseRepo>();
  NotificationSettings? settings;
  FlutterLocalNotificationsPlugin? flutterLocalNotificationsPlugin;

  refreshToken() async {
    await messageInstance?.deleteToken();
    messageInstance = FirebaseMessaging.instance;
    return await messageInstance?.getToken();
  }

  @override
  void onInit() {
    foregroundListener();
    FirebaseMessaging.onBackgroundMessage(backgroundListener);
    messageInstance?.setForegroundNotificationPresentationOptions(
        alert: true, badge: true, sound: true);
    super.onInit();
  }

  Future<String?> fcmToken() async {
    if (messageInstance != null) {
      String? token = await messageInstance?.getToken();
      log(token ?? 'null', name: 'FCM');
      return token;
    } else {
      return null;
    }
  }

  void deleteFcmToken() => messageInstance?.deleteToken;

  Future<void> initNotifications({bool isBackground = false}) async {
    messageInstance = FirebaseMessaging.instance;

    if (!isBackground) {
      settings = await messageInstance?.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );
      log('User granted permission: ${settings?.authorizationStatus}');
    }
    RemoteMessage? message = await messageInstance?.getInitialMessage();
    if (message != null) {}

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    NotificationAppLaunchDetails? notificationAppLaunchDetails =
        await flutterLocalNotificationsPlugin
            ?.getNotificationAppLaunchDetails();
    if (notificationAppLaunchDetails != null &&
        notificationAppLaunchDetails.didNotificationLaunchApp) {}
    if (GetPlatform.isAndroid) {
      flutterLocalNotificationsPlugin!
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()!
          .requestPermission();
    }
    var android = const AndroidInitializationSettings('@drawable/ic_stat_logo');
    var ios = const DarwinInitializationSettings();
    var initSettings = InitializationSettings(android: android, iOS: ios);
    flutterLocalNotificationsPlugin?.initialize(initSettings,
        onDidReceiveNotificationResponse: (details) {
      log(details.payload ?? "", name: "PAYLOAD");
      try {
        Map<String, dynamic> data = jsonDecode(details.payload!);
        log(data.toString(), name: 'data');
        log('message');
        ResponseModel? model = ResponseModel.fromJson(data);
      } catch (e) {
        log(e.toString(), name: 'error');
      }
    });

    log('User granted permission: ${settings?.authorizationStatus}');
    // fireBase();
  }

  void showNotification({
    String? title,
    String? summaryText,
    String? payload,
    String? body,
    int? id,
  }) async {
    var android = AndroidNotificationDetails(
      'Njoy',
      'njoy_channel',
      importance: Importance.max,
      priority: Priority.max,
      groupKey: 'njoy_group',
      setAsGroupSummary: true,
      enableVibration: true,
      groupAlertBehavior: GroupAlertBehavior.all,
      styleInformation: InboxStyleInformation(
        [body ?? ""],
        contentTitle: title,
        summaryText: summaryText,
      ),
    );
    var ios = const DarwinNotificationDetails();
    var platform = NotificationDetails(android: android, iOS: ios);
    await flutterLocalNotificationsPlugin!
        .show(id ?? 1, title, body, platform, payload: payload);
  }

  void foregroundListener() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      // log('Got a message whilst in the foreground!');
      // ResponseModel notificationModel =
      //     ResponseModel.fromJson(message.data);
      // try {
      //   HomeController homeController = Get.find<HomeController>();
      //   refrehNotfication(homeController);
      // } catch (e) {}
      // showNotification(
      //     title: notificationModel.title,
      //     summaryText: notificationModel.body,
      //     payload: jsonEncode(notificationModel.data),
      //     body: notificationModel.body);
      // if (message.notification != null) {
      //   log('Message also contained a notification: ${message.notification}');
      // }
    });
  }
}
