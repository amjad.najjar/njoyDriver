import 'dart:developer';

import 'package:get/get.dart';
import 'package:njoy_driver/core/models/auth_user.dart';
import 'package:njoy_driver/core/models/send_user.dart';
import 'package:njoy_driver/core/repostries/data_base_repo.dart';
import 'package:njoy_driver/core/routes/nav.dart';

import '../models/enums/role.dart';
import '../repostries/backend_repo.dart';
import 'app_controller.dart';
import 'notification.dart';

class AuthService extends GetxService {
  BackEndRepo backEndRepo = Get.find<BackEndRepo>();
  DataBaseRepo dataBaseRepo = Get.find<DataBaseRepo>();
  NotificationService notifications = Get.find<NotificationService>();
  AppService appService = Get.find<AppService>();

  @override
  void onInit() {
    super.onInit();
  }

  bool get isAuth => appService.role.value?.isAuthenticated ?? false;

  logOut() async {
    await BackEndRepo.logout();
    dataBaseRepo.tokenBox?.delete("token");
    dataBaseRepo.userBox?.delete("user");
    Nav.offAll(Pages.login);
  }

  login(SendUser user) async {
    await BackEndRepo.login(user);
    // appService.setToken(auth.token);
    // dataBaseRepo.saveUser(auth.user);
    // appService.setRole(Role.user);
  }
}
