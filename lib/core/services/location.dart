import 'dart:developer';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:njoy_driver/core/repostries/backend_repo.dart';

import '../models/enums/role.dart';
import 'app_controller.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LocationService extends GetxService {
  late bool serviceIsEnabled;
  late LocationPermission permission;
  LatLng? lastKnownLocation;
  AppService appService = Get.find<AppService>();
  late Stream<Position> locationStream;

  @override
  void onInit() async {
    serviceIsEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceIsEnabled) {
      return;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied ||
        permission == LocationPermission.deniedForever) {
      permission = await Geolocator.requestPermission();
    }
    super.onInit();
  }

  @override
  void onReady() async {
    log(permission.name);
    log(serviceIsEnabled.toString());
    if ((permission != LocationPermission.denied ||
            permission != LocationPermission.deniedForever) &&
        serviceIsEnabled) {
      await getInitialLocation();
      if (appService.getRole == Role.user) {
        locationListener();
      }
    }
    super.onReady();
  }

  getInitialLocation() async {
    Position locationData = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.medium);
    lastKnownLocation = LatLng(locationData.latitude, locationData.longitude);
    return lastKnownLocation;
  }

  locationListener() async {
    locationStream = Geolocator.getPositionStream(
        locationSettings: LocationSettings(
            distanceFilter: 10,
            timeLimit: 2.seconds,
            accuracy: LocationAccuracy.medium));
    locationStream.listen((pos) async {
      LatLng point = LatLng(pos.latitude, pos.longitude);
      Fluttertoast.showToast(msg: pos.toString());
      await BackEndRepo.changeLocation(point);
    });
  }
}
