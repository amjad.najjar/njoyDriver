import 'dart:async';
import 'dart:developer';

import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/repostries/data_base_repo.dart';
import 'package:njoy_driver/core/services/app_controller.dart';
import 'package:njoy_driver/core/services/location.dart';

class BackgroundService extends GetxService {
  init() async {
    final service = FlutterBackgroundService();
    await service.configure(
        iosConfiguration: IosConfiguration(),
        androidConfiguration: AndroidConfiguration(
            autoStart: true,
            autoStartOnBoot: true,
            onStart: myService,
            isForegroundMode: false));
  }

  @pragma('vm:entry-point')
  static myService(ServiceInstance instance) async {
    try {
      Get.put(DataBaseRepo());
      Get.put(AppService());
      Get.put<LocationService>(LocationService());
    } catch (e) {
      log(e.toString(), name: "Background service");
    }
  }

  @override
  void onInit() {
    init();
    super.onInit();
  }
}
