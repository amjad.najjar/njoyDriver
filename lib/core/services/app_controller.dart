import 'dart:async';
import 'dart:developer';
import 'package:get/get.dart';
import 'package:njoy_driver/core/repostries/backend_repo.dart';
import '../constatnts/defaults.dart';
import '../models/enums/role.dart';
import '../repostries/data_base_repo.dart';
import '../routes/nav.dart';
import '../translations/localization.dart';
import '../models/user.dart';
import 'auth_service.dart';

class AppService extends GetxService {
  DataBaseRepo dataBaseRepo = Get.find<DataBaseRepo>();
  Rx<Role?> role = Rx<Role?>(Role.guest);
  Role? get getRole => role.value;
  String token = '';
  String? fcmToken = '';

  final Rx<AppLocalization> _locale = Defaults.defaultLocale.obs;
  AppLocalization get locale => _locale.value;
  set locale(AppLocalization value) => _locale.value = value;
  @override
  void onInit() async {
    await loadData();
    changeLanguageListener();
    super.onInit();
  }

  updateLocale() async {
    if (locale == AppLocalization.ar) {
      Get.updateLocale(AppLocalization.en.locale);
      setLocale(AppLocalization.en);
    } else {
      Get.updateLocale(AppLocalization.ar.locale);
      await setLocale(AppLocalization.ar);
    }
    BackEndRepo.changeLanguage(locale);
  }

  setRole(Role role) {
    User? user = dataBaseRepo.user;
    this.role.value = role;

    if (user != null) {
      user.role = role;
      dataBaseRepo.saveUser(user);
    } else {
      dataBaseRepo.saveUser(User(role: role));
    }
  }

  setLocale(AppLocalization locale) async {
    await dataBaseRepo.saveLocale(locale.value);
    Get.updateLocale(locale.locale);
    this.locale = locale;
  }

  setToken(String token) {
    dataBaseRepo.saveToken(token);
  }

  User get userData => dataBaseRepo.user!;
  loadData() async {
    //TODO : ADD locale
    User? user = dataBaseRepo.user;
    String? locale = dataBaseRepo.locale;

    AppLocalization locality = AppLocalization.fromString("ar");
    this.locale = locality;
    role.value = dataBaseRepo.token != null ? Role.user : Role.newUser;

    token = dataBaseRepo.token ?? "";
    logger();
  }

  logger() {
    log(token, name: 'Token');
    log(locale.value, name: 'Locale');
    log(role.value.toString(), name: 'Role');
  }

  changeLanguageListener() {
    isArabic(_locale.value == AppLocalization.ar);
    isEnglish(_locale.value == AppLocalization.en);

    ever(_locale, (val) {
      if (val == AppLocalization.ar) {
        isEnglish(false);
        isArabic(true);
      }
      if (val == AppLocalization.en) {
        isEnglish(true);
        isArabic(false);
      }
    });
  }

  Future logOut() async {
    AuthService auth = Get.find<AuthService>();
    setRole(Role.guest);
    Get.back();
    await auth.logOut();
    Get.put(AuthService());
  }

  String getInitialRoute() {
    switch (role.value) {
      case Role.user:
        return Pages.home.route;
      case Role.newUser:
      case Role.guest:
      case Role.unregisteredUser:
        return Pages.login.route;
      default:
        return Pages.login.route;
    }
  }

  RxBool isEnglish = RxBool(true);
  RxBool isArabic = RxBool(false);
}
