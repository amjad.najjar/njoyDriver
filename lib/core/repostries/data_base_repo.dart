import 'dart:developer';

import 'package:get/state_manager.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import '../models/enums/role.dart';
import '../models/user.dart';

class DataBaseRepo extends GetxService {
  Box<User>? userBox;
  Box<String>? tokenBox;

  Box<String>? localeBox;
  Box<String>? notifications;

  // @override
  // void onInit()async{
  //   await init();
  //   super.onInit();
  // }
  Future init() async {
    await Hive.initFlutter();
    Hive.registerAdapter<Role>(RoleAdapter());
    Hive.registerAdapter<User>(UserAdapter());

    userBox = await Hive.openBox('user');
    localeBox = await Hive.openBox('locale');
    tokenBox = await Hive.openBox('token');
    notifications = await Hive.openBox('notifications');
  }

  saveToken(String token) => tokenBox?.put('token', token);

  saveUser(User user) => userBox?.put('user', user);

  saveLocale(String locale) => localeBox?.put('locale', locale);

  User? get user => userBox?.get('user');

  String? get token => tokenBox?.get("token");

  String? get locale => localeBox?.get('locale');
}
