import 'dart:developer';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' hide MultipartFile;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:dio/dio.dart';
import 'package:njoy_driver/core/models/auth_user.dart';
import 'package:njoy_driver/core/models/driver.dart';
import 'package:njoy_driver/core/models/reasons.dart';
import 'package:njoy_driver/core/models/send_user.dart';
import 'package:njoy_driver/core/models/trip.dart';
import 'package:njoy_driver/core/models/user.dart';
import 'package:njoy_driver/core/network/api.dart';
import 'package:njoy_driver/core/translations/localization.dart';
import '../models/response_model.dart';

class BackEndRepo extends GetxService {
  RxBool isOnline = true.obs;
  @override
  void onInit() {
    super.onInit();
    Connectivity().onConnectivityChanged.listen((event) {
      if (event == ConnectivityResult.none) {
        isOnline(false);
      } else {
        isOnline(true);
      }
    });
  }

  static Future<List<Reasons>?> getReasons() async =>
      await API.getRaisons.get<Reasons>(parser: Reasons.fromJson, isList: true);

  static Future<Trip> cancelTrip(
      {required int tripId, required int reasonId}) async {
    return await API.rejectTrips.post<Trip>(
        parser: Trip.fromJson, body: {'id': tripId, 'reason_id': reasonId});
  }

  static Future login(SendUser user) async =>
      await API.login.post<ResponseModel>(
        body: user.toJson(),
      );

  static Future changeLocation(LatLng point) async =>
      await API.changeLocation.post(
          body: {"lat": point.latitude, "lng": point.longitude},
          withDecoding: false);

  static Future veriffy(SendUser user) async {
    log("message");
    return await API.veriffy
        .post<AuthUser>(body: user.toJson(), parser: AuthUser.fromJson);
  }

  static Future<ResponseModel?> notification(
          int page, CancelToken cancelToken) async =>
      await API.notifications.get(
          params: {'page': page}, cancel: cancelToken, withDecoding: false);

  static Future<Trip> acceptTrip(int id) async =>
      await API.acceptTrips.post<Trip>(parser: Trip.fromJson, body: {"id": id});
  static Future<Trip> startTrip(int id) async =>
      await API.startTrip.post<Trip>(parser: Trip.fromJson, body: {"id": id});
  static Future<Trip> finishTrip(int id) async =>
      await API.finishTrip.post<Trip>(parser: Trip.fromJson, body: {"id": id});
  static Future<User> changeOnlineStatus() async =>
      await API.changeStatus.get<User>(parser: User.fromJson);
  static Future<ResponseModel> changeLanguage(AppLocalization locale) async =>
      await API.changeLanguage
          .post(withDecoding: false, body: {"language": locale.name});

  static Future logout() async => await API.logout.get(withDecoding: false);
}
