import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

mixin CodeVerification on GetxController {
  TextEditingController code = TextEditingController();
  GlobalKey<FormState> codeVerificationForm = GlobalKey<FormState>();
  StreamController<ErrorAnimationType>? error =
      StreamController<ErrorAnimationType>();
  RxString hasError = ''.obs;
  final int minutes = 2;
  late Rx<int> _seconds;
  int get minute => _seconds.value % 60;
  int get seconds => _seconds.value ~/ 60;
  bool get isTimerEnded => _seconds.value == 0;
  String get time =>
      "${"$seconds".padLeft(2, "0")} : ${"$minute".padLeft(2, '0')}";
  late Timer _timer;
  @override
  void onInit() {
    _seconds = Rx<int>(minutes * 60);
    _timer = Timer.periodic(1.seconds, (timer) {
      if (_seconds.value==0) {
        timer.cancel();
      } else {
        _seconds(_seconds.value - 1);
      }
    });
    super.onInit();
  }

  Future verify() async {
    if (codeVerificationForm.currentState?.validate() != true) {
      return;
    }
  }
}
