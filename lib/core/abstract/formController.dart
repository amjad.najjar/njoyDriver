import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/errors/app_error.dart';

mixin FormMixin implements GetxController {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final RxMap<String, String> _errors = RxMap<String, String>({});

  String? getError(String key) => _errors[key];

  Future request();

  Future onSubmit() async {
    if (formKey.currentState?.validate() != true) {
      return;
    } else {
      try {
        await request();
        log("send");
        _errors.clear();
      } on AppException catch (e) {
        log(e.toString());
        if (e.type == ExceptionType.validationError) {
          Map<String, List<dynamic>>? tmp = e.response?.errors;
          tmp?.forEach((key, value) {
            _errors[key] = value.join(",");
          });
        } else {
          _errors.clear();
          log(e.toString(), name: "Error");
        }
        rethrow;
      }
    }
  }
}
