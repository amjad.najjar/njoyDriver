import 'package:flutter/material.dart';
import 'package:get/get.dart';

mixin TabControllerMixin on GetxController, GetSingleTickerProviderStateMixin {
  late TabController tabController;
  abstract int length;
  RxInt selectedIndex = RxInt(0);
  @override
  void onInit() {
    tabController = TabController(vsync: this, length: length);
    bindings();
    tabListener();
    super.onInit();
  }

  tabListener() {
    tabController.addListener(() => selectedIndex(tabController.index));
  }

  bindings() {}
}
