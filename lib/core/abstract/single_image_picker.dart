import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:njoy_driver/gen/assets.gen.dart';
import 'package:njoy_driver/widgets/image.dart';

mixin SingleImageUploader on GetxController {
  Rx<ImageType> imageType = ImageType.Asset.obs;
  RxString imagePath = Assets.images.splash.path.obs;
  Future uploadImage() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    if (image == null) {
      return;
    }
    imageType(ImageType.File);
    imagePath(image.path);
  }
}
