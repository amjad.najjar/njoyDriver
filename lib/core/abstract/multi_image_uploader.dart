import 'package:dio/dio.dart';
import 'package:get/get.dart' hide FormData;
import 'package:image_picker/image_picker.dart';

mixin MultiImageUploader on GetxController {
  RxList<String> images = RxList<String>([]);
  Future uploadImages() async {
    final ImagePicker _picker = ImagePicker();
    final List<XFile> _images = await _picker.pickMultiImage();
    if (_images.isEmpty) {
      return;
    }
    _images.forEach((e) => images.add(e.path));
  }

  FormData data = FormData.fromMap({});
  deleteImage(int index) {
    images.removeAt(index);
  }
}
