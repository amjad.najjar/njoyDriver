import 'dart:developer';
import 'package:get/get.dart';
import '../utils/observable_variable.dart';

mixin FetchData<T> on GetxController {
  Future<T?> fetch();
  ObsVar<T?> data = ObsVar<T?>(null);

  Future fetchData() async {
    try {
      data.value = await fetch();
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void onReady() {
    fetchData();
    super.onReady();
  }
}
