import 'dart:async';
import 'dart:math';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

mixin MapMixin on GetxController {
  Completer<GoogleMapController> mapCompleter = Completer();
  RxSet<Marker> markers = RxSet<Marker>({});
  RxSet<Circle> circles = RxSet({});
  CameraPosition startingPosition =
      const CameraPosition(target: LatLng(33.5138, 36.2765), zoom: 12);

  double getZoomLevel(double radius) {
    double zoomLevel = 11;
    if (radius > 0) {
      double radiusElevated = radius + radius / 2;
      double scale = radiusElevated / 500;
      zoomLevel = 16 - log(scale) / log(2);
    }
    zoomLevel = double.parse(zoomLevel.toStringAsFixed(2));
    return zoomLevel;
  }

  Future goToMyLocation() async {
    GoogleMapController gController = await mapCompleter.future;
    await gController
        .animateCamera(CameraUpdate.newCameraPosition(startingPosition));
  }

  Future<Uint8List?> getBytesFromAsset(String path) async {
    double pixelRatio = Get.pixelRatio;
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: pixelRatio.round() * 30);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        ?.buffer
        .asUint8List();
  }

  bool isInCircle(LatLng point, LatLng center, double radius) {
    var ky = 40000 / 360;
    var kx = cos(pi * center.latitude / 180.0) * ky;
    var dx = (center.longitude - point.longitude).abs() * kx;
    var dy = (center.latitude - point.latitude).abs() * ky;
    return sqrt(dx * dx + dy * dy) <= radius;
  }

  LatLngBounds boundsFromLatLngList(List<LatLng> list) {
    double? x0, x1, y0, y1;
    for (LatLng latLng in list) {
      if (x0 == null) {
        x0 = x1 = latLng.latitude;
        y0 = y1 = latLng.longitude;
      } else {
        if (latLng.latitude > x1!) x1 = latLng.latitude;
        if (latLng.latitude < x0) x0 = latLng.latitude;
        if (latLng.longitude > y1!) y1 = latLng.longitude;
        if (latLng.longitude < y0!) y0 = latLng.longitude;
      }
    }
    return LatLngBounds(
        northeast: LatLng(x1!, y1!), southwest: LatLng(x0!, y0!));
  }
}
