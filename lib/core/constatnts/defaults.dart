import 'package:flutter/services.dart';
import '../models/enums/role.dart';
import '../translations/localization.dart';

abstract class Defaults {
  static const AppLocalization defaultLocale = AppLocalization.ar;

  static const Role defaultRole = Role.unregisteredUser;

  static const String appTitle = 'Njoy';

  static const String googleMapKey = 'AIzaSyBp0SxbUvHqGUR61Irbb7fzf0A3M7n5rlg';

  static const double latitude = 33.510414;

  static const double longitude = 36.278336;

  static get preferredOrientation => SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
}
