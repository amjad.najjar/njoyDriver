import 'package:flutter/material.dart' hide BoxShadow;

abstract class AppConstants {
  static EdgeInsetsGeometry contentPadding =
      const EdgeInsets.symmetric(horizontal: 24);
}
