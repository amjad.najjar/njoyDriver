import 'package:get/get.dart';

abstract class Validator {
  static String? email(String? val) {
    if (notEmpty(val) != null) {
      return notEmpty(val);
    }

    if (!RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(val!)) {
      return 'Email is invalid'.tr;
    } else {
      return null;
    }
  }

  static String? password(String? val) {}
  static String? notEmpty(String? val) {
    if (val == null || val == "") {
      return "This Felid is Required".tr;
    }
    return null;
  }

  static String? phone(String? val) {
    if (notEmpty(val) != null) {
      return notEmpty(val);
    }
    if (!RegExp(r'(^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$)')
        .hasMatch(val!)) {
      return 'Phone number is invalid'.tr;
    } else {
      return null;
    }
  }

  static String? code(String? val) {
    if (notEmpty(val) != null) {
      return notEmpty(val);
    } else {
      if (val?.length != 4 || !(val?.isNumericOnly ?? true)) {
        return 'Invalid Code'.tr;
      } else {
        return null;
      }
    }
  }

  static String? ageValidation(String? val) {
    if (notEmpty(val) != null) {
      return notEmpty(val);
    } else {
      if (!(val?.isNumericOnly ?? true)) {
        return 'Invalid Number'.tr;
      } else {
        int? age = int.parse(val!);
        if (age < 15) {
          return "15 years old is the minimum".tr;
        }
        return null;
      }
    }
  }
}
