import 'dart:developer';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart' hide FormData, MultipartFile, Response;
import 'package:njoy_driver/core/errors/app_error.dart';
import 'package:njoy_driver/core/models/response_model.dart';
import 'package:njoy_driver/core/network/api.dart';
import 'package:njoy_driver/core/repostries/data_base_repo.dart';
import 'package:njoy_driver/core/services/app_controller.dart';

class Network {
  static final DataBaseRepo _dataBaseRepo = Get.find<DataBaseRepo>();
  static final AppService appService = Get.find<AppService>();

  static Future<ResponseModel?> _performRequest(
    RequestOptions options,
  ) async {
    final watch = Stopwatch();
    Dio client = Dio(
      BaseOptions(
          baseUrl: API.baseUrl,
          sendTimeout: 80.seconds,
          connectTimeout: 80.seconds),
    );
    watch.reset();
    Response dioResponse;
    try {
      watch.start();
      dioResponse = await client.request(
        options.baseUrl + options.path,
        data: options.data,
        options: Options(method: options.method, headers: options.headers),
        queryParameters: options.queryParameters,
        cancelToken: options.cancelToken,
        onSendProgress: options.onSendProgress,
        onReceiveProgress: options.onReceiveProgress,
      );
      watch.stop();
    } on DioException catch (ex) {
      log(ex.toString());
      if (ex.response!.statusCode == 400) {
        return Future.error(AppException(
            type: ExceptionType.flowError,
            response: ResponseModel.fromJson(ex.response?.data)));
      }
      if (ex.response!.statusCode == 500 ||
          ex.response!.statusCode == 401 ||
          ex.response!.statusCode == 422 ||
          ex.response!.statusCode == 403) {
        Fluttertoast.showToast(msg: '${ex.response?.data.toString()}');
        return Future.error(AppException(
            type: ExceptionType.validationError,
            response: ResponseModel.fromJson(ex.response?.data)));
      }
      switch (ex.type) {
        case DioExceptionType.connectionError:
        case (DioExceptionType.connectionTimeout ||
              DioExceptionType.receiveTimeout ||
              DioExceptionType.sendTimeout):
          return Future.error(
              AppException(type: ExceptionType.connectionError));

        case DioExceptionType.badResponse:
          if (ex.response?.statusCode == 401) {
            Fluttertoast.showToast(msg: '${ex.response?.data.toString()}');
            await appService.logOut();
            return Future.error(
                AppException(type: ExceptionType.unauthenticated));
          }
          if (ex.response!.statusCode == 500) {
            log("kokokoo");
            return null;
          } else if (ex.response?.statusCode == 422) {
            Fluttertoast.showToast(msg: '${ex.response?.data}');
            return Future.error(AppException(
                statusCode: ex.response?.statusCode,
                response: ResponseModel.fromJson(ex.response?.data),
                type: ExceptionType.validationError));
          } else {
            Fluttertoast.showToast(msg: '${ex.response?.data}');
          }

        case DioExceptionType.unknown:
          ScaffoldMessenger.of(Get.context!).showSnackBar(SnackBar(
              content: Text(
            "connection error".tr,
            style:
                Get.context!.textTheme.bodySmall!.copyWith(color: Colors.white),
          )));

        default:
          ScaffoldMessenger.of(Get.context!).showSnackBar(SnackBar(
              content: Text(
            "${"Unknown Error".tr} ${ex.response?.statusCode}",
            style:
                Get.context!.textTheme.bodySmall!.copyWith(color: Colors.white),
          )));
      }
      return Future.error(ex);
    } catch (ex) {
      ScaffoldMessenger.of(Get.context!).showSnackBar(SnackBar(
          content: Text(
        "NETWORK Error, Check your internet connection...!",
        style: Get.context!.textTheme.bodySmall!.copyWith(color: Colors.white),
      )));

      return Future.error(ex);
    } finally {
      watch.stop();
    }
    return dioResponse.data is String
        ? ResponseModel.fromJson({})
        : ResponseModel.fromJson(dioResponse.data);
  }

  static Future<ResponseModel?> get(
      {required String url,
      bool isFullUrl = false,
      Map<String, dynamic>? queryParams,
      CancelToken? cancel,
      Map<String, String>? additionalHeaders}) async {
    String? token = _dataBaseRepo.token;
    try {} catch (ex) {}
    if (token == null) {}
    try {
      String normalizedToken = token ?? "";
      Map<String, String> localizationHeaders = {
        'Accept-Language': _dataBaseRepo.locale ?? 'ar'
      };
      Map<String, String> jsonHeaders = {
        'Content-Type': "application/json",
        "Accept": "application/json",
      };
      Map<String, String> authHeader = {
        'Authorization': "Bearer $normalizedToken",
      };
      Map<String, String> headers = {};
      headers.addAll(additionalHeaders ?? {});
      headers.addAll(authHeader);
      headers.addAll(jsonHeaders);
      headers.addAll(localizationHeaders);

      var request = RequestOptions(
          method: 'get',
          baseUrl: isFullUrl ? null : API.baseUrl,
          validateStatus: (status) => true,
          path: url,
          queryParameters: queryParams,
          headers: headers,
          cancelToken: cancel);
      dynamic response;
      response = await _performRequest(request);
      return response;
    } on FormatException {
      return Future.error(FormatException);
    } catch (e) {
      rethrow;
    }
  }

  static Future<ResponseModel?> delete(
      {required String url,
      bool isFullUrl = false,
      Map<String, dynamic>? queryParams,
      CancelToken? cancel,
      Map<String, String>? additionalHeaders}) async {
    String? token = _dataBaseRepo.token;
    if (token == null) {}
    try {
      String normalizedToken = token ?? "";
      Map<String, String> localizationHeaders = {
        'Accept-Language': _dataBaseRepo.locale ?? 'ar'
      };
      Map<String, String> jsonHeaders = {
        'Content-Type': "application/json",
        "Accept": "application/json",
      };
      Map<String, String> authHeader = {
        'Authorization': "Bearer $normalizedToken",
      };
      Map<String, String> headers = {};
      headers.addAll(additionalHeaders ?? {});
      headers.addAll(authHeader);
      headers.addAll(jsonHeaders);
      headers.addAll(localizationHeaders);

      var request = RequestOptions(
          method: 'delete',
          baseUrl: isFullUrl ? null : API.baseUrl,
          validateStatus: (status) => true,
          path: url,
          queryParameters: queryParams,
          headers: headers,
          cancelToken: cancel);
      dynamic response;
      try {
        response = await _performRequest(request);
        // log(response.data.toString());
      } on DioError {
        // return errorHandler(ex);
      }
      return response;
    } on FormatException {
      return Future.error(FormatException);
    } catch (e) {
      rethrow;
    }
  }

  static Future<ResponseModel?> post(
      {required String url,
      required dynamic parameters,
      CancelToken? cancel,
      FormData? files}) async {
    String? token = _dataBaseRepo.token;
    var data = files ?? parameters;
    try {
      String normalizedToken = token ?? "";

      var request = RequestOptions(
          method: 'post',
          validateStatus: (status) => true,
          baseUrl: API.baseUrl,
          path: url,
          cancelToken: cancel,
          headers: {
            'Authorization': "Bearer $normalizedToken",
            'Content-Type': "application/json",
            "Accept": "application/json",
            "Accept-Language": _dataBaseRepo.locale ?? 'ar'
          },
          data: data);
      ResponseModel? response;
      try {
        response = await _performRequest(request);
      } on DioError {
        log('dio error');
      }
      return response;
    } on FormatException {
      log('message');
      return Future.error(FormatException);
    } catch (e, stackTrace) {
      log(stackTrace.toString());
      return Future.error(e);
    }
  }
}
