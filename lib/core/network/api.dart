import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:njoy_driver/core/errors/app_error.dart';
import '../errors/parsing.dart';
import '../models/response_model.dart';
import 'network.dart';

abstract class API {
  static String baseUrl = "https://taxinjoy.com/driver/";

  //=========== Auth ============
  static String login = "drivers/login";
  static String logout = "drivers/logout";
  static String veriffy = "drivers/verify";

  //=========== Profile ===========
  static String getProfile = "drivers/get";
  static String notifications = "notifications/get";
  static String changeStatus = "drivers/changeOnlineStatus";
  static String changeLanguage = "drivers/changeLanguage";
  static String changeLocation = "drivers/changeLocation";

  //=========== Trips ===========
  static String acceptTrips = "trips/accept";
  static String rejectTrips = "trips/skip";
  static String finishTrip = "trips/finish";
  static String startTrip = "trips/start";
  static String getRaisons = 'reasons/all';
  static String rejectReason = "reasons/all";
  static String trips = "trips/tripsByStatus";
}

extension APIExtension on String {
  Future<dynamic> get<T>(
      {Map<String, dynamic>? params,
      Function(Map<String, dynamic> json)? parser,
      bool isFullUrl = false,
      bool withDecoding = true,
      CancelToken? cancel,
      bool isList = false}) async {
    ResponseModel? response = await Network.get(
        url: this, queryParams: params, cancel: cancel, isFullUrl: isFullUrl);
    if (!withDecoding) {
      return response;
    }
    dynamic data = response?.data;
    dynamic decodedData;

    parser ??= ResponseModel.fromJson;
    try {
      if (isList) {
        List? listData = data;
        decodedData = List<T>.of(listData!.map<T>((db) => parser?.call(db)));
      } else {
        decodedData = parser(data);
      }
      // response?.data = decodedData;
      // log('Parser : Successfully Parsed to ${T.toString()}');

      return (T is ResponseModel) ? response : decodedData;
    } catch (e, stacktrace) {
      log(e.toString());
      log(stacktrace.toString());
      throw (AppException(
          response: response, type: ExceptionType.parsingError));
    }
  }

  Future<dynamic> delete<T>(
      {Map<String, dynamic>? params,
      Function(Map<String, dynamic> json)? parser,
      bool isFullUrl = false,
      bool withDecoding = true,
      CancelToken? cancel,
      bool isList = false}) async {
    ResponseModel? response = await Network.delete(
        url: this, queryParams: params, cancel: cancel, isFullUrl: isFullUrl);
    if (!withDecoding) {
      return response;
    }
    dynamic data = response?.data;
    dynamic decodedData;

    parser ??= ResponseModel.fromJson;
    try {
      if (isList) {
        List? listData = data;
        decodedData = List<T>.of(listData!.map<T>((db) => parser?.call(db)));
      } else {
        decodedData = parser(data);
      }
      // response?.data = decodedData;
      // log('Parser : Successfully Parsed to ${T.toString()}');

      return (T is ResponseModel) ? response : decodedData;
    } catch (e, stacktrace) {
      log(stacktrace.toString());
      throw (AppException(
          response: response, type: ExceptionType.parsingError));
    }
  }

  Future<dynamic> post<T>(
      {Function(Map<String, dynamic> json)? parser,
      FormData? files,
      Map<String, dynamic>? body,
      bool withDecoding = true,
      CancelToken? cancel,
      bool isList = false}) async {
    ResponseModel? response = await Network.post(
        url: this, cancel: cancel, parameters: body, files: files);
    if (!withDecoding) {
      return response;
    }
    dynamic data = response?.data;
    // log("response data: $data");

    dynamic decodedData;
    parser ??= ResponseModel.fromJson;
    // log("response data: $data");
    try {
      if (isList) {
        List? listData = data;
        decodedData = List<T>.of(listData!.map<T>((db) => parser?.call(db)));
      } else {
        decodedData = data != null ? parser(data) : null;
      }
      // response?.data = decodedData;
      // log('Parser : Successfully Parsed to ${T.toString()}');
      return decodedData;
    } catch (e, stacktrace) {
      rethrow;
      log(stacktrace.toString());
      // throw (AppException(
      //     response: response, type: ExceptionType.parsingError));
    }
  }
}
