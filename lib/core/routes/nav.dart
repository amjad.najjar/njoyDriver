import 'package:get/get.dart';
import 'package:njoy_driver/screens/auth/code_verffication/controller.dart';
import 'package:njoy_driver/screens/auth/code_verffication/index.dart';
import 'package:njoy_driver/screens/auth/controller.dart';
import 'package:njoy_driver/screens/auth/index.dart';
import 'package:njoy_driver/screens/home/controller.dart';
import 'package:njoy_driver/screens/home/index.dart';
import 'package:njoy_driver/screens/map/controller.dart';
import 'package:njoy_driver/screens/map/index.dart';
import 'package:njoy_driver/screens/notifications/controller.dart';
import 'package:njoy_driver/screens/notifications/index.dart';
import 'package:njoy_driver/screens/profile/controller.dart';
import 'package:njoy_driver/screens/profile/index.dart';
import 'package:njoy_driver/screens/splash/controller.dart';
import 'package:njoy_driver/screens/splash/index.dart';
import 'package:njoy_driver/screens/trip_details/controller.dart';
import 'package:njoy_driver/screens/trip_details/index.dart';

abstract class AppRouting {
  static List<GetPage<dynamic>> routes() => [
        GetPage(
            name: Pages.login.route,
            bindings: [BindingsBuilder.put(() => LoginController())],
            page: () => const LoginPage()),
        GetPage(
            name: Pages.home.route,
            bindings: [BindingsBuilder.put(() => HomeController())],
            page: () => const HomePage()),
        GetPage(
            name: Pages.tripDetails.route,
            bindings: [BindingsBuilder.put(() => TripDetailsController())],
            page: () => const TripDetailsPage()),
        GetPage(
            name: Pages.notifications.route,
            bindings: [BindingsBuilder.put(() => NotificationsController())],
            page: () => const NotificationsPage()),
        GetPage(
            name: Pages.profile.route,
            bindings: [BindingsBuilder.put(() => ProfileController())],
            page: () => const ProfilePage()),
        GetPage(
            name: Pages.map.route,
            bindings: [BindingsBuilder.put(() => MapController())],
            page: () => const MapPage()),
        GetPage(
            name: Pages.otpCodeVerffication.route,
            bindings: [BindingsBuilder.put(() => CodeVerfficationController())],
            page: () => const CodeVerfficationPage()),
        GetPage(
            name: Pages.splash.route,
            bindings: [BindingsBuilder.put(() => SplashController())],
            page: () => const SplashPage()),
      ];
}

enum Pages {
  login,
  tripDetails,
  notifications,
  otpCodeVerffication,
  profile,
  splash,
  map,
  home;

  String get route => '/$name';
}

abstract class Nav {
  static Future? to(Pages page,
          {dynamic arguments, Map<String, String>? params}) =>
      Get.toNamed(page.route, arguments: arguments, parameters: params);

  static Future? replacement(Pages page,
          {dynamic arguments, Map<String, String>? params}) =>
      Get.offNamed(page.route, arguments: arguments, parameters: params);

  static Future? offAll(Pages page,
          {dynamic arguments, Map<String, String>? params}) =>
      Get.offAllNamed(page.route, arguments: arguments, parameters: params);

  static Future? toHome() {
    return Get.offNamedUntil(
        Pages.home.route, (route) => route.settings.name == Pages.home.route);
  }
}
