import 'package:flutter/material.dart';

extension TimeExtensions on TimeOfDay? {
  Duration? substract(TimeOfDay? other) {
    if (this != null && other != null) {
      int hours = (this!.hour) - other.hour;
      int minutes = (this!.minute) - other.minute;
      if (minutes < 0) {
        hours = hours - 1;
        minutes = 60 + minutes;
      }
      return Duration(hours: hours, minutes: minutes);
    } else {
      return null;
    }
  }
}
//2:30 3:20