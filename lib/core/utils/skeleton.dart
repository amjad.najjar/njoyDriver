import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../styles/colors.dart';

extension Skeleton on Widget {
  Widget toSkeleton(int length, {double? width, double? separator}) {
    return Shimmer.fromColors(
      baseColor: AppColors.grey.withOpacity(.25),
      highlightColor: AppColors.primary,
      child: SizedBox(
          width: width,
          child: ListView.separated(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              itemCount: length,
              itemBuilder: (_, index) => this,
              separatorBuilder: (_, index) => SizedBox(height: separator))),
    );
  }
}
