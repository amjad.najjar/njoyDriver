class ListDecoder<T> {
  late dynamic json;
  late T Function(Map<String, dynamic> json) parser;
  ListDecoder({required this.json, required this.parser});

  List<T>? jsonToList() {
    return json != null
        ? List<T>.of(json.map<T>((item) => parser(item)))
        : null;
  }
}
