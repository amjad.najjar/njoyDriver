import 'package:get/get.dart';

extension NumbersExtension on num {
  String get toMoney {
    num number = this;
    if (number >= 1000) {
      String num = '$number';
      int start = 0;
      if (num.length % 3 != 0) {
        start++;
        if ((num.length - 1) % 3 != 0) {
          start++;
          if ((num.length - 2) % 3 != 0) {
            start++;
          }
        }
      }
      String s = '';
      if (start != 0) {
        s = num.substring(0, start);
      }
      for (int i = start; i + 3 <= num.length; i += 3) {
        String num1 = num.substring(i, i + 3);
        if (i != 0) s += ",";
        s += num1;
      }
      return s + " " + "s.p".tr;
    }
    return '$number ${'s.p'.tr}';
  }
}
