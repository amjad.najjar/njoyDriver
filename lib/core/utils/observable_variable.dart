import 'package:get/get.dart';
import '../models/enums/variable_status.dart';

class Obs {
  Rx<VariableStatus>? _status;
  String? _error;
  set error(value) {
    _error = value;
    if (value != null) {
      status = VariableStatus.error;
    }
  }

  Rx<VariableStatus>? get listener => _status;
  String? get error => _error;

  VariableStatus? get status => _status?.value;
  set status(VariableStatus? value) => _status?.value = value!;

  bool get hasData => status == VariableStatus.hasData;
  bool get hasError => status == VariableStatus.error;
  bool get loading => status == VariableStatus.loading;
  refresh() => _status?.refresh();
}

class ObsVar<T> extends Obs {
  late T _value;

  ObsVar(T val) {
    if (val is List) {
      throw 'use ObsList instead ObsVar';
    }
    _value = val;
    if (_value == null) {
      _status = (VariableStatus.loading).obs;
    } else {
      _status = (VariableStatus.hasData).obs;
    }
  }

  T get value => _value;
  set value(val) {
    _value = val;
    if (_value == null) {
      status = VariableStatus.loading;
    } else {
      status = VariableStatus.hasData;
    }
  }
}

class ObsList<T> extends Obs {
  late List<T?>? _value;
  RxInt? _valueLength;
  int? get valueLength => _valueLength?.value;
  set valueLength(value) => _valueLength?.value = value;

  ObsList(List<T?>? value) {
    _value = value;
    if (_value == null || _value!.isEmpty) {
      _status = (VariableStatus.loading).obs;
      _valueLength = 0.obs;
    } else {
      _status = (VariableStatus.hasData).obs;
      _valueLength = (value?.length ?? 0).obs;
    }
  }

  List<T?>? get value => _value;
  set value(List<T?>? value) {
    if (value == null) {
      status = VariableStatus.loading;
    } else if (value.isEmpty) {
      status = VariableStatus.error;
      error = 'لا يوجد بيانات';
    } else {
      status = VariableStatus.hasData;
      _value = value;
      valueLength = value.length;
    }
  }

  set valueAppend(List<T> value) {
    if (status != VariableStatus.hasData) {
      status = VariableStatus.hasData;
    }
    _value?.addAll(value);
    if (_value!.isEmpty) _status = (VariableStatus.hasData).obs;
    valueLength = (valueLength ?? 0) + value.length;
  }

  removeAt(int index) {
    assert(index < (valueLength ?? 0));
    _value?.removeAt(index);
    valueLength = (valueLength ?? 1) - 1;
  }

  get reset {
    _value = [];
    valueLength = 0;
    status = VariableStatus.loading;
  }
}
