import 'package:flutter/widgets.dart';

extension Responsive on BoxConstraints {
  bool get isMobile => maxWidth < 576;
  bool get isTablet => maxWidth < 800 && maxWidth > 576;
  bool get isDisktop => maxWidth > 800;
}
