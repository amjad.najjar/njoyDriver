import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

extension StringExtension on String {
  String capitalizeFirst() {
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }

  TimeOfDay toTime() {
    final format = DateFormat.jm(); //"6:00 AM"
    return TimeOfDay.fromDateTime(format.parse(this));
  }

  String toDoubleDigits() {
    if (this.length > 1) {
      return this;
    } else {
      return "0$this";
    }
  }
}
