import 'package:get/get.dart';
import 'package:njoy_driver/core/repostries/backend_repo.dart';
import 'package:njoy_driver/core/services/app_controller.dart';
import 'package:njoy_driver/core/services/auth_service.dart';
import 'package:njoy_driver/screens/profile/widgets/confirmation_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileController extends GetxController {
  AppService appService = Get.find();
  AuthService authService = Get.find();
  RxBool isOnline = true.obs;
  changeOnlineStatus() {
    Get.dialog(
      ConfirmationDialog(
        title: 'Attention'.tr,
        onSubmit: () async {
          await BackEndRepo.changeOnlineStatus();
          isOnline(!isOnline.value);
        },
        description: "Do you want to change your online status ?".tr,
      ),
    );
  }

  changeLanguage() {
    Get.dialog(
      ConfirmationDialog(
        title: 'Attention'.tr,
        onSubmit: () async {
          await appService.updateLocale();
        },
        description: "Do you want to change language ?".tr,
      ),
    );
  }

  logout() {
    Get.dialog(
      ConfirmationDialog(
        title: 'Attention'.tr,
        onSubmit: () async => await authService.logOut(),
        description: "Do you want to logout ?".tr,
      ),
    );
  }

  callSupport() async =>
      await launchUrl(Uri(scheme: 'tel', path: '0987654321'));
}
