import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/styles/colors.dart';

import '../../../widgets/button.dart';

class ConfirmationDialog extends StatelessWidget {
  final String title;
  final String description;
  final Function onSubmit;

  const ConfirmationDialog(
      {Key? key,
      required this.title,
      required this.description,
      required this.onSubmit})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.all(32),
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 32),
              Text(
                title,
                style: Get.textTheme.headlineMedium,
              ),
              SizedBox(height: 26),
              Text(
                description,
                style: Get.textTheme.bodyLarge?.copyWith(color: AppColors.grey),
              ),
              SizedBox(height: 32),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Row(
                  children: [
                    Expanded(
                      child: AppButton(
                        text: 'Cancel'.tr,
                        type: ButtonType.secondary,
                        withLoading: false,
                        onTap: () async => Get.back(),
                      ),
                    ),
                    const SizedBox(width: 16),
                    Expanded(
                      child: AppButton(
                        text: 'Submit'.tr,
                        withLoading: false,
                        onTap: () async {
                          await onSubmit.call();
                          Get.back();
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 12),
            ],
          ),
        ),
      ),
    );
  }
}
