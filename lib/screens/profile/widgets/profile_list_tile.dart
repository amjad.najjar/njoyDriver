import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/styles/colors.dart';

class ProfileListTile extends StatelessWidget {
  final String title;
  final Function callBack;
  final Widget? trailing;
  const ProfileListTile(
      {Key? key, required this.title, required this.callBack, this.trailing})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callBack(),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: Get.textTheme.labelMedium,
            ),
            trailing ??
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: Get.theme.scaffoldBackgroundColor,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Icon(
                    Icons.arrow_forward_ios_rounded,
                    size: 18,
                  ),
                )
          ],
        ),
      ),
    );
  }
}
