import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/services/app_controller.dart';
import 'package:njoy_driver/screens/profile/controller.dart';
import 'package:njoy_driver/screens/profile/widgets/confirmation_dialog.dart';
import 'package:njoy_driver/screens/profile/widgets/profile_list_tile.dart';
import 'package:njoy_driver/widgets/app_bar.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePage extends GetView<ProfileController> {
  const ProfilePage({super.key});
  @override
  Widget build(BuildContext context) {
    AppService appService = Get.find();
    return Scaffold(
      appBar: CustomAppBar("profile".tr),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(children: [
          ProfileListTile(
            title: 'call support'.tr,
            callBack: () async => await controller.callSupport(),
          ),
          const SizedBox(height: 16),
          ProfileListTile(
            title: 'change status'.tr,
            trailing: Obx(() => Switch(
                value: controller.isOnline.value,
                onChanged: (val) => controller.changeOnlineStatus())),
            callBack: () async => controller.changeOnlineStatus(),
          ),
          SizedBox(height: 16),
          ProfileListTile(
            title: 'language @param'
                .trParams({'param': appService.locale.asString()}),
            callBack: () => controller.changeLanguage(),
          ),
          SizedBox(height: 16),
          ProfileListTile(
              title: 'logout'.tr, callBack: () async => controller.logout()),
        ]),
      ),
    );
  }
}
