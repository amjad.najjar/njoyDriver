import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/services/app_controller.dart';

import '../../core/routes/nav.dart';
import '../../styles/colors.dart';

class SplashController extends GetxController
    with GetSingleTickerProviderStateMixin {
  late AnimationController animationController;
  late Animation<Color?> colorAnimation;
  late Animation<Color?> iconColor;
  late Animation positon;

  AppService appService = Get.find();
  late CurvedAnimation curve;
  @override
  void onInit() {
    animationController = AnimationController(vsync: this, duration: 4.seconds);
    curve = CurvedAnimation(parent: animationController, curve: Curves.linear);
    colorAnimation = ColorTween(begin: AppColors.black, end: AppColors.white)
        .animate(animationController);
    iconColor =
        ColorTween(begin: AppColors.white, end: AppColors.black).animate(curve);
    // positon = Tween<Offset>(begin: );
    animationController.repeat();
    2.seconds.delay(() => Get.offAndToNamed(appService.getInitialRoute()));
    super.onInit();
  }

  @override
  void onClose() {
    animationController.dispose();
    curve.dispose();
    super.onClose();
  }
}
