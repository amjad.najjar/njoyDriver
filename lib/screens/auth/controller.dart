import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/abstract/formController.dart';
import 'package:njoy_driver/core/models/send_user.dart';
import 'package:njoy_driver/core/routes/nav.dart';
import 'package:njoy_driver/core/services/auth_service.dart';
import 'package:njoy_driver/core/services/notification.dart';

class LoginController extends GetxController with FormMixin {
  TextEditingController phone = TextEditingController();

  AuthService authService = Get.find();
  NotificationService notificationService = Get.find();

  @override
  Future request() async {
    SendUser user = SendUser(
        phone: phone.text,
        fcmToken: await notificationService.fcmToken() ?? "no");
    await authService.login(user);
    Nav.offAll(Pages.otpCodeVerffication, arguments: {"phone": phone.text});
  }
}
