import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:njoy_driver/core/constatnts/validator.dart';
import 'package:njoy_driver/screens/auth/controller.dart';
import 'package:njoy_driver/styles/colors.dart';
import 'package:njoy_driver/widgets/text_feild.dart';

import '../../widgets/button.dart';

class LoginPage extends GetView<LoginController> {
  const LoginPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Form(
      key: controller.formKey,
      child: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        children: [
          const SizedBox(height: 64),
          Text(
            'Enter your phone number'.tr,
            style: Get.textTheme.bodyMedium,
          ),
          SizedBox(height: 16),
          Text(
            'Please enter your phone number to\nusing Njoy services'.tr,
            style: const TextStyle(color: AppColors.grey),
          ),
          SizedBox(height: 25),
          Obx(() => AppTextField(
                controller.phone,
                title: 'phone'.tr,
                error: controller.getError('mobile'),
                validator: Validator.phone,
                keyboardType: TextInputType.number,
              )),
          // const SizedBox(height: 8),
          // Obx(() => AppTextField(
          //       controller.password,
          //       error: controller.errors.value['password'],
          //       title: 'password'.tr,
          //       isSecure: true,
          //       validator: Validator.notEmpty,
          //       keyboardType: TextInputType.text,
          //     )),
          const SizedBox(height: 64),
          AppButton(
            text: 'Next'.tr,
            onTap: () async => await controller.onSubmit(),
          ),
        ],
      ),
    ));
  }
}
