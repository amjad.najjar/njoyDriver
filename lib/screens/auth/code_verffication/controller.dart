import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/errors/app_error.dart';
import 'package:njoy_driver/core/errors/parsing.dart';
import 'package:njoy_driver/core/models/auth_user.dart';
import 'package:njoy_driver/core/models/enums/role.dart';
import 'package:njoy_driver/core/models/send_user.dart';
import 'package:njoy_driver/core/repostries/backend_repo.dart';
import 'package:njoy_driver/core/routes/nav.dart';
import 'package:njoy_driver/core/services/auth_service.dart';
import 'package:njoy_driver/core/services/notification.dart';

class CodeVerfficationController extends GetxController {
  GlobalKey<FormState> key = GlobalKey<FormState>();
  TextEditingController otpController = TextEditingController();
  Rx<String> _error = "".obs;
  setError(String val) => _error(val);
  String? get error => _error.value != "" ? _error.value : null;
  String phone = Get.arguments["phone"];
  AuthService authService = Get.find();
  NotificationService notificationService = Get.find();

  submitOtp() async {
    try {
      SendUser user = SendUser(
          phone: phone,
          code: otpController.text,
          fcmToken: await notificationService.fcmToken() ?? "no");
      AuthUser? auth = await BackEndRepo.veriffy(user);
      await authService.appService.setToken(auth!.token);
      await authService.appService.setRole(Role.user);

      await authService.dataBaseRepo.saveUser(auth.user);

      await Nav.to(Pages.home);
    } on AppException catch (e) {
      _error(e.response?.message ?? "");
      log(e.toString());
    }
  }
}
