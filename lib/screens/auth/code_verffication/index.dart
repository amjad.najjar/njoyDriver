import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/screens/auth/code_verffication/controller.dart';
import 'package:njoy_driver/styles/colors.dart';
import 'package:njoy_driver/styles/fonts.dart';
import 'package:njoy_driver/widgets/app_bar.dart';
import 'package:njoy_driver/widgets/button.dart';
import 'package:pinput/pinput.dart';

class CodeVerfficationPage extends GetView<CodeVerfficationController> {
  const CodeVerfficationPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(""),
      body: Form(
        key: controller.key,
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Align(
                    alignment: AlignmentDirectional.topStart,
                    child: Text(
                      'Please Verify your phone number !'.tr,
                      style: Get.textTheme.bodyMedium,
                    )),
                const Spacer(),
                Obx(() => Pinput(
                      controller: controller.otpController,
                      forceErrorState: true,
                      errorText: controller.error,
                      onChanged: (v) => controller.setError(""),
                      errorBuilder: (errorText, pin) => Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Center(child: Text(errorText ?? "")),
                      ),
                      errorTextStyle: AppFonts.bodyMedium.copyWith(),
                      //   defaultPinTheme: PinTheme(
                      //       decoration: BoxDecoration(color: AppColors.primary)),
                    )),
                const Spacer(),
                SizedBox(
                  width: 200,
                  child: AppButton(
                    text: "submit".tr,
                    onTap: () async => await controller.submitOtp(),
                  ),
                ),
                const Spacer(
                  flex: 2,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
