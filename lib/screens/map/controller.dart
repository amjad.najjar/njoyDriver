import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:njoy_driver/core/abstract/map_controller.dart';
import 'package:njoy_driver/gen/assets.gen.dart';
import '../../core/services/location.dart';

class MapController extends GetxController with MapMixin {
  LatLng src = Get.arguments['src']!;
  LatLng? dist = Get.arguments['dist'];
  LocationService locationService = Get.find();
  @override
  onReady() {
    2.seconds.delay(() => initMap());
    super.onReady();
  }

  initMap() async {
    GoogleMapController controller = await mapCompleter.future;

    Uint8List? driverIcon =
        await getBytesFromAsset(Assets.images.driverMarker.path);
    Marker driverMarker = Marker(
        position: locationService.lastKnownLocation!,
        icon: BitmapDescriptor.fromBytes(driverIcon!),
        markerId: const MarkerId('driver'));
    markers.add(driverMarker);
    Uint8List? srcIcon = await getBytesFromAsset(Assets.images.from.path);
    Marker srcMarker = Marker(
        position: src,
        icon: BitmapDescriptor.fromBytes(srcIcon!),
        markerId: const MarkerId('src'));
    markers.add(srcMarker);
    if (dist != null) {
      Uint8List? distIcon = await getBytesFromAsset(Assets.images.to.path);
      Marker distMarker = Marker(
          position: dist!,
          icon: BitmapDescriptor.fromBytes(distIcon!),
          markerId: const MarkerId('dist'));
      markers.add(distMarker);
    }
    List<LatLng> points = [locationService.lastKnownLocation!, src];
    points.addIf(dist, dist!);

    LatLngBounds bounds = boundsFromLatLngList(points);
    await controller.animateCamera(CameraUpdate.newLatLngBounds(bounds, 60));

    locationService.locationStream.listen((pos) async {
      LatLng point = LatLng(pos.latitude, pos.longitude);
      List<LatLng> points = [point, src];
      points.addIf(dist, dist!);
      markers.add(Marker(
          position: point,
          icon: BitmapDescriptor.fromBytes(driverIcon),
          markerId: const MarkerId('driver')));
      LatLngBounds bounds = boundsFromLatLngList(points);
      await controller.animateCamera(CameraUpdate.newLatLngBounds(bounds, 16));
    });
  }
}
