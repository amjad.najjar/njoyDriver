import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:njoy_driver/screens/map/controller.dart';

class MapPage extends GetView<MapController> {
  const MapPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(() => GoogleMap(
          markers: controller.markers.value,
          circles: controller.circles.value,
          zoomControlsEnabled: false,
          onMapCreated: (gController) =>
              controller.mapCompleter.complete(gController),
          initialCameraPosition: controller.startingPosition)),
    );
  }
}
