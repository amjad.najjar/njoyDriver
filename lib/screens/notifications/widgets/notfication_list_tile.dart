import 'package:flutter/material.dart' hide Badge;
import 'package:get/get.dart';
import 'package:njoy_driver/core/models/notification.dart';
import 'package:njoy_driver/core/services/app_controller.dart';
import 'package:njoy_driver/styles/colors.dart';
import 'package:badges/badges.dart';
import 'package:timeago/timeago.dart' as timeago;

class NotificationsListTile extends StatelessWidget {
  final NotificationModel notification;

  const NotificationsListTile(this.notification);
  @override
  Widget build(BuildContext context) {
    AppService appService = Get.find();
    return Badge(
      showBadge: false,
      position: BadgePosition.topEnd(top: 5, end: 0),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 8.0),
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                    notification.title ?? 'Null',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Get.textTheme.labelMedium!.copyWith(
                      color: Get.theme.colorScheme.secondary,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(width: 12),
                Text(
                  timeago.format(DateTime.parse(notification.createdAt!),
                      locale: "ar"),
                  style: TextStyle(
                    color: AppColors.grey,
                    fontSize: 10.0,
                  ),
                ),
              ],
            ),
            SizedBox(height: 6.0),
            Text(
              notification.body ?? 'null',
              maxLines: 2,
            ),
          ],
        ),
      ),
    );
  }
}
