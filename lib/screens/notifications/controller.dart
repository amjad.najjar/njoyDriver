import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/models/response_model.dart';
import 'package:njoy_driver/core/repostries/backend_repo.dart';

class NotificationsController extends GetxController {
  Future<ResponseModel?> fetchData(int page, CancelToken cancelToken) async =>
      BackEndRepo.notification(page, cancelToken);
}
