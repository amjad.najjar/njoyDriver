import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/constatnts/tags.dart';
import 'package:njoy_driver/screens/notifications/controller.dart';
import 'package:njoy_driver/screens/notifications/widgets/notfication_list_tile.dart';
import 'package:njoy_driver/widgets/app_bar.dart';

import '../../core/models/notification.dart';
import '../../core/pagination/controller.dart';
import '../../core/pagination/index.dart';

class NotificationsPage extends GetView<NotificationsController> {
  const NotificationsPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar('notifications'.tr),
      body: Pagination<NotificationModel>(
        tag: AppTags.notifications,
        refresh: true,
        padding: const EdgeInsets.symmetric(horizontal: 16),
        controller: PaginationController<NotificationModel>(
          fromJson: (json) => NotificationModel.fromJson(json),
          fetchApi: controller.fetchData,
        ),
        itemBuilder: (index, notification) =>
            NotificationsListTile(notification!),
      ),
    );
  }
}
