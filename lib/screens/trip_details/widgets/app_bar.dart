import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/models/enums/trip_state.dart';
import 'package:njoy_driver/styles/colors.dart';
import 'package:njoy_driver/styles/fonts.dart';

import '../../trip_details/controller.dart';

class DeliveryRequestAppBar extends GetView<TripDetailsController>
    implements PreferredSizeWidget {
  final Color? backgroundColor;
  final Color? foregroundColor;
  const DeliveryRequestAppBar(
      {this.backgroundColor, this.foregroundColor, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Obx(() => Container(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              decoration: BoxDecoration(
                color: controller.trip.value.state.color,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  IconButton(
                      padding: EdgeInsets.zero,
                      constraints: BoxConstraints.tight(Size(32, 32)),
                      onPressed: () => Get.back(),
                      icon: Icon(Icons.arrow_back,
                          color: foregroundColor ?? AppColors.white)),
                  Spacer(),
                  if (controller.trip.value.state != TripState.pendding)
                    Text(controller.trip.value.state.toString(),
                        style: Get.textTheme.bodyMedium?.copyWith(
                            color: foregroundColor ?? AppColors.white,
                            fontWeight: AppFontWeight.light)),
                  Expanded(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(controller.trip.value.type.name,
                          style: Get.textTheme.bodyLarge?.copyWith(
                              color: foregroundColor ?? AppColors.white)),
                      Text(
                        '#${controller.trip.value.id}',
                        style: Get.textTheme.bodyLarge?.copyWith(
                          color: foregroundColor ?? AppColors.white,
                        ),
                      ),
                    ],
                  ))
                ],
              ),
            )));
  }

  @override
  Size get preferredSize => const Size.fromHeight(160);
}
