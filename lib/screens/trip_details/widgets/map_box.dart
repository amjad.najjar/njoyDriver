import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:njoy_driver/screens/trip_details/controller.dart';

class MapBox extends GetView<TripDetailsController> {
  const MapBox({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16),
        child: Obx(() => GoogleMap(
              compassEnabled: true,
              myLocationEnabled: true,
              myLocationButtonEnabled: true,
              zoomControlsEnabled: false,
              gestureRecognizers: Set()
                ..add(Factory<EagerGestureRecognizer>(
                    () => EagerGestureRecognizer())),
              onMapCreated: (gController) =>
                  controller.mapCompleter.complete(gController),
              markers: controller.markers.value,
              circles: controller.circles.value,
              initialCameraPosition: controller.startingPosition,
            )),
      ),
    );
  }
}
