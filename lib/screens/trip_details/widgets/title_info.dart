import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/screens/home/widgets/trip_card.dart';
import 'package:njoy_driver/styles/colors.dart';

class TripInfoDetails extends StatelessWidget {
  final String title;
  final String? info;
  final Widget? icon;
  final Widget? trailing;
  final Function? onTap;

  const TripInfoDetails(
      {required this.title,
      this.info,
      this.trailing,
      this.onTap,
      this.icon,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (info == null) return SizedBox.shrink();
    return GestureDetector(
      onTap: () => onTap?.call(),
      child: Container(
        margin: const EdgeInsets.only(bottom: 12.0),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
              offset: Offset(3, 3),
              color: Colors.grey.withOpacity(.2),
              blurRadius: 12)
        ], color: AppColors.white, borderRadius: BorderRadius.circular(8)),
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16),
        child: TripInfo(
            title: title,
            info: info!,
            icon: icon,
            trailing: trailing,
            style: Get.textTheme.labelMedium?.copyWith(fontSize: 17)),
      ),
    );
  }
}
