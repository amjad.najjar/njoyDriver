import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/models/enums/trip_state.dart';
import 'package:njoy_driver/screens/trip_details/controller.dart';
import 'package:njoy_driver/widgets/button.dart';

class ChangeTripState extends GetView<TripDetailsController> {
  const ChangeTripState({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      switch (controller.trip.value.state) {
        case TripState.pendding:
          return AppButton(
            text: "accept".tr,
            onTap: () async => controller.acceptTrip(),
          );
        case TripState.onGoing:
          return AppButton(
              color: TripState.completed.color,
              text: "finish".tr,
              onTap: () async => controller.finishTrip());

        case TripState.accepted:
          return Row(
            children: [
              Expanded(
                  child: AppButton(
                text: "cancel".tr,
                onTap: () async => await controller.cancel(),
                color: TripState.canceled.color,
              )),
              const SizedBox(
                width: 16,
              ),
              Expanded(
                child: AppButton(
                    text: "user arrived".tr,
                    color: TripState.onGoing.color,
                    onTap: () async => controller.startTrip()),
              )
            ],
          );
        default:
          return const SizedBox.shrink();
      }
    });
  }
}
