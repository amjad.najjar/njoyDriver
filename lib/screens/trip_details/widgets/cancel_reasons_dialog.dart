import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/models/reasons.dart';

import '../../../styles/colors.dart';

class CancelReasonsDialog extends StatelessWidget {
  final List<Reasons?>? reasons;
  final Function(int) onReject;
  const CancelReasonsDialog(
      {required this.reasons, required this.onReject, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScrollController controller = ScrollController();
    return Center(
      child: Container(
        padding: const EdgeInsets.all(
          16,
        ),
        constraints: BoxConstraints(
            maxWidth: Get.width * .8, maxHeight: Get.height * .6),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: AppColors.white,
        ),
        child: Material(
          color: AppColors.white,
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            SizedBox(
              height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Reasons For Cancellation'.tr,
                      style: Get.textTheme.headlineLarge?.copyWith(
                          fontSize: 22, color: Get.theme.primaryColor)),
                  IconButton(
                      onPressed: () => Get.back(), icon: Icon(Icons.close)),
                ],
              ),
            ),
            const SizedBox(height: 16),
            Expanded(
              child: Scrollbar(
                controller: controller,
                thumbVisibility: false,
                child: ListView.builder(
                  itemCount: reasons?.length,
                  controller: controller,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(reasons![index]!.name),
                      onTap: () async => await onReject(reasons![index]!.id),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      // selectedTileColor: AppColors.overlay.withOpacity(.05),
                    );
                  },
                ),
              ),
            ),
            const SizedBox(height: 16),
          ]),
        ),
      ),
    );
  }
}

class ReasonsRejectController extends GetxController {
  Rx<int> selectedReasonId = (-1).obs;
  ScrollController scroll = ScrollController();
  @override
  void onInit() {
    super.onInit();
  }
}
