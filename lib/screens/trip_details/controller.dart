import 'dart:developer';
import 'dart:typed_data';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:njoy_driver/core/abstract/map_controller.dart';
import 'package:njoy_driver/core/models/enums/trip_state.dart';
import 'package:njoy_driver/core/models/reasons.dart';
import 'package:njoy_driver/core/repostries/backend_repo.dart';
import 'package:njoy_driver/core/utils/observable_variable.dart';
import 'package:njoy_driver/gen/assets.gen.dart';
import 'package:njoy_driver/screens/trip_details/widgets/cancel_reasons_dialog.dart';
import 'package:njoy_driver/styles/theme.dart';

import '../../core/models/trip.dart';
import '../../core/services/location.dart';

class TripDetailsController extends GetxController with MapMixin {
  final Trip _trip = Get.arguments;
  late Rx<Trip> trip = Rx<Trip>(_trip);
  RxBool isLoading = false.obs;
  ObsList<Reasons> reasons = ObsList<Reasons>(null);
  LocationService locationService = Get.put(LocationService());
  Rx<LatLng> currentPoint = Rx<LatLng>(const LatLng(33.5138, 36.2765));

  @override
  onReady() {
    super.onReady();
    AppStyle.setTripStatusBarStyle(_trip.state);
    tripListener();
    getReasons();
    initMap();
  }

  initMap() async {
    GoogleMapController mapController = await mapCompleter.future;
    Uint8List? driverIcon =
        await getBytesFromAsset(Assets.images.driverMarker.path);
    markers.add(Marker(
        icon: BitmapDescriptor.fromBytes(driverIcon!),
        position: locationService.lastKnownLocation!,
        markerId: const MarkerId("Driver")));

    if (_trip.content is ShortTripContent) {
      Uint8List? srcIcon = await getBytesFromAsset(Assets.images.from.path);
      Marker src = Marker(
          icon: BitmapDescriptor.fromBytes(srcIcon!),
          position: (_trip.content as ShortTripContent).src.point,
          markerId: MarkerId("src"));
      markers.add(src);
      if (_trip.content is TaxiTrip ||
          (_trip.content is FlexTrip) && _trip.state == TripState.completed) {
        //TODO:check here
        Uint8List? distIcon = await getBytesFromAsset(Assets.images.to.path);
        Marker dist = Marker(
            icon: BitmapDescriptor.fromBytes(distIcon!),
            position: (_trip.content as TaxiTrip).dist.point,
            markerId: MarkerId("dist"));
        markers.add(dist);
      }
    }

    locationService.locationStream.listen((pos) {
      Fluttertoast.showToast(msg: "UPDATED");

      LatLng point = LatLng(pos.latitude, pos.longitude);
      currentPoint(point);
      mapController.animateCamera(CameraUpdate.newLatLng(point));
      markers.add(Marker(
          icon: BitmapDescriptor.fromBytes(driverIcon),
          position: point,
          markerId: const MarkerId("Driver")));
    });
  }

  acceptTrip() async {
    trip.value = await BackEndRepo.acceptTrip(_trip.id);
  }

  startTrip() async {
    trip.value = await BackEndRepo.startTrip(_trip.id);
  }

  finishTrip() async {
    trip.value = await BackEndRepo.finishTrip(_trip.id);
  }

  tripListener() {
    ever<Trip>(trip, (trip) {
      AppStyle.setTripStatusBarStyle(trip.state);
    });
  }

  Future cancelReservation(int tripId) async {
    trip.value = await BackEndRepo.cancelTrip(tripId: tripId, reasonId: tripId);
  }

  Future cancel() async {
    log("message");
    try {
      Get.dialog(CancelReasonsDialog(
        onReject: (int id) async => await cancelReservation(id),
        reasons: reasons.value!,
      ));
    } catch (e) {
      log(e.toString());
    }
  }

  getReasons() async {
    reasons.value = await BackEndRepo.getReasons();
  }

  @override
  void onClose() {
    AppStyle.setTransparentStatusBarStyle();
    super.onClose();
  }
}
