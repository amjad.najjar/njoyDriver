import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/models/enums/trip_state.dart';
import 'package:njoy_driver/core/models/enums/trip_type.dart';
import 'package:njoy_driver/core/models/trip.dart';
import 'package:njoy_driver/core/routes/nav.dart';
import 'package:njoy_driver/gen/assets.gen.dart';
import 'package:njoy_driver/screens/trip_details/controller.dart';
import 'package:njoy_driver/screens/trip_details/widgets/app_bar.dart';
import 'package:njoy_driver/screens/trip_details/widgets/change_trip_state.dart';
import 'package:njoy_driver/screens/trip_details/widgets/map_box.dart';
import 'package:njoy_driver/screens/trip_details/widgets/title_info.dart';
import 'package:njoy_driver/styles/colors.dart';

class TripDetailsPage extends GetView<TripDetailsController> {
  const TripDetailsPage({super.key});
  @override
  Widget build(BuildContext context) {
    Trip trip = controller.trip.value;
    return Scaffold(
      appBar: const DeliveryRequestAppBar(),
      body: ListView(
        padding: const EdgeInsets.all(16),
        children: [
          // UserInfo(),
          Obx(() => Visibility(
              visible: controller.trip.value.state == TripState.onGoing,
              child: SizedBox(
                  width: Get.width,
                  height: Get.height * .6,
                  child: const MapBox()))),
          TripInfoDetails(
            // onTap: () => Nav.to(Pages.map, arguments: {
            //   'type': PointType.from,
            //   'position': controller.trip.source,
            //   'trip': controller.trip
            // }),
            title: 'start point'.tr,
            info: trip.content.srcName,
            icon: Assets.icons.location.svg(color: AppColors.primary),
            trailing: Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: AppColors.border),
                ),
                child: const Center(
                    child: Icon(
                  Icons.chevron_right_sharp,
                  color: AppColors.primary,
                ))),
          ),
          if (trip.content is ShortTripContent)
            if (trip.type != TripType.flex || trip.state == TripState.completed)
              TripInfoDetails(
                onTap: () => Nav.to(Pages.map, arguments: {
                  "src": (trip.content as ShortTripContent).src.point,
                  "dist": (trip.content is TaxiTrip)
                      ? (trip.content as TaxiTrip).dist.point
                      : null
                }),
                title: 'end point'.tr,
                info: trip.distName,
                icon: Assets.icons.location.svg(color: AppColors.primary),
                trailing: Container(
                    width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: AppColors.border),
                    ),
                    child: const Center(
                        child: Icon(
                      Icons.chevron_right_sharp,
                      color: AppColors.primary,
                    ))),
              ),
          // if (trip.type == TripType.taxi)
          //   TripInfoDetails(
          //     title: 'Expected Distance'.tr,
          //     info: trip.expectedDistance.toString(),
          //     icon: SvgPicture.asset(
          //       AppAssets.route,
          //     ),
          //   ),
          // if (trip.type == TripType.taxi)
          //   TripInfoDetails(
          //     title: 'Expected Time'.tr,
          //     info: trip.expectedTime.toString(),
          //     icon: SvgPicture.asset(
          //       AppAssets.duration,
          //       height: 24,
          //     ),
          //   ),
          if ((trip.content is ShortTripContent) &&
              (trip.content as ShortTripContent).tracking != null)
            TripInfoDetails(
                title: 'Tracking'.tr,
                icon: Assets.icons.tracking.svg(height: 24)),
          if ((trip.content is ShortTripContent) &&
              (trip.content as ShortTripContent).deluxe != null)
            TripInfoDetails(
              title: 'Deluxe'.tr,
              icon: Assets.icons.gem.svg(height: 24),
            ),
          if (trip.price != null)
            TripInfoDetails(
              title: 'price'.tr,
              info: "@param s.p".trParams({'param': trip.price.toString()}),
              icon: Assets.icons.money.svg(height: 24),
            ),
          TripInfoDetails(
            title: 'Date'.tr,
            info: '2022/10/16',
            icon: Icon(Icons.calendar_month),
          ),
          TripInfoDetails(
              title: 'Time'.tr,
              info: '12:00 AM',
              icon: Assets.icons.clock.svg()),
          const Padding(
            padding: EdgeInsets.only(top: 32.0),
            child: ChangeTripState(),
          )
        ],
      ),
    );
  }
}
