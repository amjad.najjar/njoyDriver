import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/abstract/tab_controller.dart';
import 'package:njoy_driver/core/models/response_model.dart';
import 'package:njoy_driver/core/network/api.dart';
import 'package:njoy_driver/styles/colors.dart';

class HomeController extends GetxController
    with GetSingleTickerProviderStateMixin, TabControllerMixin {
  @override
  int length = 3;
  Color get tabColor {
    switch (selectedIndex.value) {
      case 0:
        return AppColors.secondary;
      case 1:
        return AppColors.orange;
      case 2:
        return AppColors.done;
      default:
        return AppColors.done;
    }
  }

  String get filter {
    switch (selectedIndex.value) {
      case 0:
        return 'current';
      case 1:
        return 'new';
      case 2:
        return 'other';
      default:
        return 'other';
    }
  }

  Future<ResponseModel?> fetchData(int page, CancelToken cancelToken) async {
    return await API.trips
        .get(params: {"page": page, "status": filter}, withDecoding: false);
  }
}
