import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/models/trip.dart';
import 'package:njoy_driver/core/pagination/index.dart';
import 'package:njoy_driver/screens/home/controller.dart';
import 'package:njoy_driver/screens/home/widgets/home_app_bar.dart';
import 'package:njoy_driver/screens/home/widgets/tabs.dart';
import '../../core/pagination/controller.dart';
import 'widgets/trip_card.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const HomeAppBar(),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 16),
              const TripTaps(),
              const SizedBox(height: 16),
              Expanded(
                child: Obx(() => Pagination(
                    refresh: true,
                    tag: controller.filter,
                    loading: const SizedBox.shrink(),
                    controller: PaginationController<Trip>(
                        fromJson: Trip.fromJson,
                        fetchApi: controller.fetchData),
                    itemBuilder: (index, trip) => TripContainer(trip: trip!))),
              )
            ],
          ),
        ));
  }
}
