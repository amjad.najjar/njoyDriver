import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/models/trip.dart';
import 'package:njoy_driver/core/routes/nav.dart';
import 'package:njoy_driver/gen/assets.gen.dart';
import 'package:njoy_driver/styles/fonts.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../core/models/enums/trip_state.dart';
import '../../../core/models/enums/trip_type.dart';
import '../../../styles/colors.dart';

class TripContainer extends StatelessWidget {
  final Trip trip;

  const TripContainer({
    required this.trip,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16),
      child: GestureDetector(
        onTap: () => Nav.to(Pages.tripDetails, arguments: trip),
        child: Container(
          padding: const EdgeInsets.only(bottom: 12),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            color: AppColors.white,
          ),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TripHeader(
                    state: trip.state, type: trip.type!, id: '${trip.id}'),
                const SizedBox(height: 12),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          child: TripInfo(
                        icon: Assets.icons.location.svg(
                            colorFilter: const ColorFilter.mode(
                                AppColors.primary, BlendMode.srcIn)),
                        title: 'From'.tr,
                        info: trip.content.srcName,
                      )),
                      const SizedBox(width: 12),
                    ],
                  ),
                ),
                SizedBox(height: 12),
                if (trip.type == TripType.taxi)
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: TripInfo(
                      icon: Assets.icons.location.svg(
                          colorFilter: const ColorFilter.mode(
                              AppColors.primary, BlendMode.srcIn)),
                      title: 'To'.tr,
                      info: trip.distName ?? "-",
                    ),
                  ),
                SizedBox(height: 12),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: TripInfo(
                      icon: Assets.icons.routing.svg(
                          colorFilter: const ColorFilter.mode(
                              AppColors.primary, BlendMode.srcIn)),
                      title: 'expected distance'.tr,
                      info: '@param K.M'
                          .trParams({"param": trip.distance.toString()})),
                ),
                SizedBox(height: 12),
              ]),
        ),
      ),
    );
  }
}

class TripHeader extends StatelessWidget {
  final TripState state;
  final String id;
  final TripType type;
  const TripHeader(
      {required this.state, required this.type, required this.id, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
          color: state.color,
          borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(5),
              topRight: const Radius.circular(5))),
      child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        Text('#$id',
            style: GoogleFonts.righteous(color: AppColors.white, fontSize: 18)),
        const SizedBox(
          width: 8,
        ),
        Text(state.toString(),
            style: GoogleFonts.righteous(
                color: AppColors.white,
                fontSize: 18,
                fontWeight: AppFontWeight.semiBold)),
        const Spacer(),
        SvgPicture.asset(
          type.icon,
          width: 30,
          color: AppColors.white,
        ),
      ]),
    );
  }
}

class TripInfo extends StatelessWidget {
  final Widget? icon;
  final String title;
  final String info;
  final TextStyle? style;
  final Widget? trailing;
  const TripInfo(
      {this.icon,
      required this.title,
      required this.info,
      this.trailing,
      this.style,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        if (icon != null) icon!,
        if (icon != null) const SizedBox(width: 16),
        Container(
          constraints: BoxConstraints(minWidth: 60, maxWidth: Get.width * .4),
          child: Text('$title :',
              style: (style ?? Get.textTheme.labelMedium)?.copyWith(
                  fontWeight: AppFontWeight.semiBold,
                  color: AppColors.orderInfo)),
        ),
        const Spacer(),
        Expanded(
          flex: 4,
          child: Text(
            info,
            style: (style ?? Get.textTheme.labelMedium)
                ?.copyWith(fontWeight: AppFontWeight.semiBold),
          ),
        ),
        trailing ?? const SizedBox.shrink()
      ],
    );
  }
}
