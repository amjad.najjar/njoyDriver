import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/services/app_controller.dart';
import 'package:njoy_driver/gen/assets.gen.dart';
import 'package:njoy_driver/styles/colors.dart';
import 'package:njoy_driver/styles/fonts.dart';
import 'package:njoy_driver/widgets/image.dart';

import '../../../core/routes/nav.dart';

class HomeAppBar extends StatelessWidget implements PreferredSizeWidget {
  const HomeAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppService appService = Get.find();
    return SafeArea(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: AppColors.divider))),
        child: GestureDetector(
          onTap: () => Nav.to(Pages.profile),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CircleAvatar(
                radius: 30,
                child: AppImage(
                  path: appService.userData.img ?? '',
                  border: Border.all(color: AppColors.avatar, width: 2),
                  width: 60,
                  height: 60,
                  borderRadius: BorderRadius.circular(30),
                  type: ImageType.CachedNetwork,
                ),
              ),
              const SizedBox(width: 16),
              Text(
                '${appService.userData!.name}\n${appService.userData.phone}',
                style: Get.textTheme.labelLarge?.copyWith(
                  fontWeight: AppFontWeight.semiBold,
                ),
              ),
              const Spacer(),
              IconButton(
                onPressed: () => Nav.to(Pages.notifications),
                icon: Assets.icons.notification.svg(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(Get.height * .2);
}
