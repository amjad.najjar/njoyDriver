import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/screens/home/controller.dart';
import 'package:njoy_driver/styles/colors.dart';

class TripTaps extends GetView<HomeController> {
  const TripTaps({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Obx(
          () => TabBar(
              controller: controller.tabController,
              labelStyle: Get.textTheme.bodyMedium,
              unselectedLabelColor: AppColors.black,
              indicatorSize: TabBarIndicatorSize.tab,
              labelColor: AppColors.white,
              indicator: BoxDecoration(
                  color: controller.tabColor,
                  borderRadius: BorderRadius.circular(5)),
              tabs: [
                Tab(text: 'current'.tr),
                Tab(text: 'today'.tr),
                Tab(text: 'done'.tr),
              ]),
        ));
  }
}
