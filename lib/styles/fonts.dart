import 'package:flutter/material.dart';

abstract class AppFonts {
  static TextStyle displayLarge = TextStyle(
    fontWeight: AppFontWeight.semiBold,
    fontSize: 34,
  );

  static TextStyle displayMedium = TextStyle(
    fontWeight: AppFontWeight.medium,
    fontSize: 34,
  );
  static TextStyle displaySmall = TextStyle(
    fontWeight: AppFontWeight.semiBold,
    fontSize: 32,
  );

  static TextStyle headlineLarge = TextStyle(
    fontWeight: AppFontWeight.bold,
    fontSize: 32,
  );
  static TextStyle headlineMedium = TextStyle(
    fontWeight: AppFontWeight.semiBold,
    fontSize: 32,
  );
  static TextStyle headlineSmall = TextStyle(
    fontWeight: AppFontWeight.medium,
    fontSize: 32,
  );
  static TextStyle titleLarge = TextStyle(
    fontWeight: AppFontWeight.medium,
    fontSize: 30,
  );
  static TextStyle titleMedium = TextStyle(
    fontWeight: AppFontWeight.semiBold,
    fontSize: 28,
  );
  static TextStyle titleSmall = TextStyle(
    fontWeight: AppFontWeight.medium,
    fontSize: 28,
  );
  static TextStyle bodyLarge = TextStyle(
    fontWeight: AppFontWeight.medium,
    fontSize: 24,
  );
  static TextStyle bodyMedium = TextStyle(
    fontWeight: AppFontWeight.medium,
    fontSize: 20,
  );
  static TextStyle bodySmall = TextStyle(
    fontWeight: AppFontWeight.medium,
    fontSize: 12,
  );

  static TextStyle labelLarge = TextStyle(
    fontWeight: AppFontWeight.medium,
    fontSize: 18,
  );
  static TextStyle labelMedium = TextStyle(
    fontWeight: AppFontWeight.medium,
    fontSize: 16,
  );
  static TextStyle labelSmall = TextStyle(
    fontWeight: AppFontWeight.medium,
    fontSize: 14,
  );
  static ButtonStyle get greyTextButtonStyle => ButtonStyle(
        visualDensity: VisualDensity.compact,
        padding: const MaterialStatePropertyAll<EdgeInsetsGeometry>(
            EdgeInsets.symmetric(horizontal: 8)),
        // foregroundColor: MaterialStatePropertyAll<Color>(AppColors.secondary),
        textStyle: MaterialStatePropertyAll<TextStyle>(AppFonts.titleMedium),
      );
}

abstract class AppFontWeight {
  static FontWeight light = FontWeight.w300;
  static FontWeight regular = FontWeight.w400;
  static FontWeight medium = FontWeight.w500;
  static FontWeight semiBold = FontWeight.w700;
  static FontWeight bold = FontWeight.w800;
}
