import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color primary = Color(0xFF5BBA6F);
  static const Color scaffold = Color(0xFFF3F3F3);
  static const Color secondary = Color(0xFF145770);
  static const Color divider = Color(0xFFD9D9D9);

  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF000000);

  static const Color avatar = Color(0xFF292D32);

  static const Color done = Color(0xFF00C851);
  static const Color refused = Color(0xFFFF3838);
  static const Color pending = Color(0xFFFFB302);

  static const Color orderInfo = Color(0xFFAFB0AF);
  static const Color softText = Color(0xFF707070);
  static const Color appBarColorTake = Color(0xFFFFE27D);
  static const Color appBarColorDeliver = Color(0xFF2F6778);
  static const Color red = Color(0xFFD81721);
  static const Color orange = Color(0xffFFAA00);
  static const Color redDivider = Color(0xFFE2312F);
  static const Color border = Color(0xFFE0E0DF);
  static const Color grey = Color(0xFF838484);
}
