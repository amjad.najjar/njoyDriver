import 'package:flutter/material.dart';
import 'fonts.dart';

abstract class AppInputFieldThemes {
  static InputDecorationTheme mainTheme = InputDecorationTheme(
    contentPadding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
    border: InputBorder.none,
    labelStyle: AppFonts.bodyMedium,
    hintStyle: AppFonts.bodyMedium,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    disabledBorder: InputBorder.none,
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
      // borderSide: BorderSide(color: redColor),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
      // borderSide: BorderSide(color: redColor),
    ),
  );
}
