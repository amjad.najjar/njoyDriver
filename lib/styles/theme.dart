import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/models/enums/trip_state.dart';
import 'colors.dart';
import 'fonts.dart';
import 'inputs.dart';

abstract class AppStyle {
  static ThemeData mainTheme = ThemeData(
      primaryColor: AppColors.primary,
      tabBarTheme: TabBarTheme(dividerColor: Colors.transparent),
      drawerTheme: const DrawerThemeData(
          surfaceTintColor: AppColors.white,
          backgroundColor: AppColors.white,
          width: 300,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadiusDirectional.only(
                bottomEnd: Radius.circular(28), topEnd: Radius.circular(28)),
          )),
      scrollbarTheme: ScrollbarThemeData(
        trackVisibility: MaterialStateProperty.all(false),
        thumbVisibility: MaterialStateProperty.all(true),
        thickness: MaterialStateProperty.all(18),
        radius: const Radius.circular(32),
        thumbColor: MaterialStateProperty.all(const Color(0xFF4CF992)),
      ),
      appBarTheme: AppBarTheme(
          surfaceTintColor: AppColors.primary,
          actionsIconTheme: const IconThemeData(size: 12),
          backgroundColor: Colors.transparent,
          iconTheme: const IconThemeData(color: AppColors.secondary),
          titleTextStyle:
              AppFonts.titleMedium.copyWith(color: AppColors.secondary)),
      scaffoldBackgroundColor: AppColors.scaffold,
      useMaterial3: true,
      checkboxTheme: CheckboxThemeData(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          fillColor: MaterialStatePropertyAll<Color>(AppColors.primary),
          checkColor: MaterialStatePropertyAll<Color>(AppColors.white)),
      radioTheme: RadioThemeData(
        fillColor: MaterialStateProperty.all<Color>(AppColors.primary),
      ),
      switchTheme: SwitchThemeData(
          thumbColor: MaterialStateProperty.resolveWith(
        (states) => states.contains(MaterialState.selected)
            ? AppColors.scaffold
            : AppColors.grey,
      )),
      dialogBackgroundColor: AppColors.white,
      dialogTheme: const DialogTheme(
          backgroundColor: AppColors.white,
          surfaceTintColor: Colors.transparent),
      colorScheme: ColorScheme.fromSwatch().copyWith(
          secondary: const Color(0xFF4CF992), primary: const Color(0xFF30D28E)),
      textButtonTheme: TextButtonThemeData(style: AppFonts.greyTextButtonStyle),
      bottomNavigationBarTheme: const BottomNavigationBarThemeData(
          type: BottomNavigationBarType.fixed,
          elevation: 0,
          landscapeLayout: BottomNavigationBarLandscapeLayout.centered,
          backgroundColor: Colors.transparent),
      bottomSheetTheme:
          const BottomSheetThemeData(backgroundColor: Colors.transparent),
      dividerColor: AppColors.border,
      fontFamily: 'ExpoArabic',
      textTheme: TextTheme(
        displayLarge: AppFonts.displayLarge,
        displayMedium: AppFonts.displayMedium,
        displaySmall: AppFonts.displaySmall,
        headlineLarge: AppFonts.headlineLarge,
        headlineMedium: AppFonts.headlineMedium,
        headlineSmall: AppFonts.headlineSmall,
        titleLarge: AppFonts.titleLarge,
        titleMedium: AppFonts.titleMedium,
        titleSmall: AppFonts.titleSmall,
        bodyLarge: AppFonts.bodyLarge,
        bodyMedium: AppFonts.bodyMedium,
        bodySmall: AppFonts.bodySmall,
        labelLarge: AppFonts.labelLarge,
        labelMedium: AppFonts.labelMedium,
        labelSmall: AppFonts.labelSmall,
      ),
      inputDecorationTheme: AppInputFieldThemes.mainTheme);

  static setPrimaryStatusBarStyle() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: AppColors.primary,
      statusBarIconBrightness: Brightness.dark,
    ));
  }

  static setSecondaryStatusBarStyle() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: mainTheme.colorScheme.secondary,
      statusBarIconBrightness: Brightness.light,
    ));
  }

  static setTransparentStatusBarStyle() {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ));
  }

  static setTripStatusBarStyle(TripState state) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: state.color,
      statusBarIconBrightness: Brightness.light,
    ));
  }
}
