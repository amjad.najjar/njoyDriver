import 'dart:developer';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:njoy_driver/core/services/background_service.dart';
import 'core/repostries/backend_repo.dart';
import 'core/repostries/data_base_repo.dart';
import 'core/services/app_controller.dart';
import 'core/services/auth_service.dart';
import 'core/services/location.dart';
import 'core/services/notification.dart';
import 'core/translations/localization.dart';
import 'firebase_options.dart';
import 'styles/theme.dart';
import 'core/routes/nav.dart';

void main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  AppStyle.setTransparentStatusBarStyle();
  DataBaseRepo db = Get.put(DataBaseRepo());
  NotificationService notificationService = Get.put(NotificationService());
  try {
    await notificationService.initNotifications();
  } catch (e) {}
  try {
    BackEndRepo backEnd = Get.put(BackEndRepo());

    await db.init();
  } catch (e) {}
  AppService app = Get.put(AppService());
  BackgroundService backgroundService = Get.put(BackgroundService());
  Get.put(AuthService());
  String initialRoute = app.getInitialRoute();
  log(initialRoute, name: 'Initial route');
  // FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  runApp(Obx(() => MyApp(
        initialRoute: initialRoute,
        locale: app.locale.locale,
      )));
}

class MyApp extends StatelessWidget {
  final String initialRoute;
  final Locale locale;
  const MyApp({required this.initialRoute, required this.locale, super.key});

  @override
  Widget build(BuildContext context) {
    AppStyle.setTransparentStatusBarStyle();

    return GetMaterialApp(
      title: 'Njoy',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        DefaultCupertinoLocalizations.delegate,
      ],
      translations: Messages(),
      locale: locale,
      fallbackLocale: Locale('en', 'US'),
      supportedLocales: <Locale>[
        Locale('en', 'US'),
        Locale('ar', 'AE'),
      ],
      theme: AppStyle.mainTheme,
      getPages: AppRouting.routes(),
      initialRoute: Pages.splash.route,
      initialBinding: InitialBindings(),
    );
  }
}

class InitialBindings implements Bindings {
  @override
  void dependencies() async {
    Get.put(NotificationService());
    Get.put(LocationService());
  }
}
