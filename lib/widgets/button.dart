import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import '../core/models/enums/widget_state.dart';
import '../styles/colors.dart';
import '../styles/fonts.dart';

class AppButton extends StatelessWidget {
  final String text;
  final String? iconUrl;
  final ButtonType type;
  final Function? onTap;
  final Color? borderColor;
  final double? width;
  final double? fontSize;
  final Color? foregroundColor;
  bool withLoading;
  final Color? color;
  final EdgeInsets? padding;
  final Rx<WidgetState> _widgetState = WidgetState.interaction.obs;
  WidgetState get widgetState => _widgetState.value;
  set widgetState(WidgetState value) => _widgetState.value = value;

  AppButton(
      {super.key,
      required this.text,
      this.type = ButtonType.primary,
      this.iconUrl,
      this.onTap,
      this.withLoading = true,
      this.width,
      this.fontSize,
      this.padding,
      this.borderColor,
      this.color,
      this.foregroundColor});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () async {
          if (widgetState == WidgetState.loading || type == ButtonType.disabled)
            return;
          if (withLoading) {
            widgetState = WidgetState.loading;
            try {
              await onTap!();
            } catch (e) {}
            widgetState = WidgetState.interaction;
          } else {
            onTap!();
          }
        },
        child: Container(
            constraints: const BoxConstraints(maxWidth: 800, maxHeight: 60),
            width: width,
            padding: padding ?? const EdgeInsets.symmetric(vertical: 16),
            decoration: BoxDecoration(
                color: color ?? type.getBackGroundColor(),
                borderRadius: BorderRadius.circular(50),
                border: type.border),
            child: Obx(
              () => Center(
                child: widgetState == WidgetState.loading
                    ? SizedBox(
                        height: 30,
                        child: SpinKitThreeBounce(
                          size: 20,
                          color: type.getForegroundColor(),
                        ),
                      )
                    : iconUrl != null
                        ? Row(mainAxisSize: MainAxisSize.min, children: [
                            SvgPicture.asset(
                              iconUrl!,
                              height: 20,
                              // width: 24,
                              color: type.getForegroundColor(),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(
                              text,
                              style: AppFonts.labelLarge
                                  .copyWith(color: type.getForegroundColor()),
                            )
                          ])
                        : Text(text,
                            style: AppFonts.labelLarge.copyWith(
                                fontSize: fontSize,
                                color: foregroundColor ??
                                    type.getForegroundColor())),
              ),
            )));
  }
}

enum ButtonType {
  primary,
  secondary,
  disabled,
  bordered,
  white,
  textOnly;

  Border? get border {
    switch (this) {
      case bordered:
        return Border.all(color: AppColors.primary);
      default:
        return null;
    }
  }

  Color? getBackGroundColor() {
    switch (this) {
      case primary:
        return AppColors.primary;
      case secondary:
        return AppColors.secondary;
      case textOnly:
        return Colors.transparent;
      case disabled:
        return AppColors.primary;
      case bordered:
      case white:
        return AppColors.white;
    }
  }

  Color? getForegroundColor() {
    switch (this) {
      case disabled:
      case primary:
        return AppColors.white;
      case secondary:
        return AppColors.white;
      case textOnly:
        return AppColors.secondary;
      case white:
        return AppColors.secondary;
      case bordered:
        return AppColors.primary;
    }
  }
}
