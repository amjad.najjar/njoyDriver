import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:njoy_driver/gen/assets.gen.dart';
import '../styles/colors.dart';

class AppTextField extends StatefulWidget {
  final TextEditingController controller;
  final String? title;
  final String? icon;
  final String? prefixIcon;
  final Widget? suffixIcon;
  String? error;
  final String? initialValue;
  final TextInputType? keyboardType;
  final bool isSecure;
  final double? height;
  final double? width;
  final Function()? onTap;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? contentPadding;
  final bool isEnabled;
  final String? Function(String?)? validator;
  final TextStyle? textStyle;
  final Color? color;
  final TextAlign textAlign;
  final TextInputAction? textInputAction;
  final Function? onEditingComplete;
  final bool expand;
  final BoxConstraints? constraints;
  final int? maxLines;
  AppTextField(this.controller,
      {this.title,
      this.padding,
      this.height,
      this.textInputAction,
      this.onEditingComplete,
      this.suffixIcon,
      this.width,
      this.onTap,
      this.textStyle,
      this.error,
      this.maxLines = 1,
      this.isEnabled = true,
      this.contentPadding,
      this.isSecure = false,
      this.expand = false,
      this.constraints,
      this.keyboardType,
      this.textAlign = TextAlign.start,
      this.icon,
      this.color,
      this.prefixIcon,
      this.initialValue,
      this.validator,
      Key? key})
      : super(key: key);

  @override
  State<AppTextField> createState() => _AppTextFieldState();
}

class _AppTextFieldState extends State<AppTextField> {
  final FocusNode focusNode = FocusNode();
  bool hasFocus = false;
  bool passwordIsShwon = true;
  @override
  void initState() {
    if (widget.isSecure) {
      passwordIsShwon = false;
    }
    focusNode.addListener(() {
      setState(() {
        hasFocus = focusNode.hasFocus;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      validator: widget.validator,
      textInputAction: widget.textInputAction,
      keyboardType: widget.keyboardType,
      obscureText: !passwordIsShwon,
      readOnly: !widget.isEnabled,
      focusNode: focusNode,
      onEditingComplete: () => widget.onEditingComplete?.call(),
      onTap: widget.onTap,
      initialValue: widget.initialValue,
      style: widget.textStyle,
      textAlign: widget.textAlign,
      expands: widget.expand,
      maxLines: widget.maxLines,
      onChanged: (_) {
        setState(() {
          widget.error = null;
        });
      },
      decoration: InputDecoration(
          errorText: widget.error,
          fillColor: widget.color,
          filled: true,
          constraints: widget.constraints,
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 25, vertical: 18),
          prefixIconConstraints:
              const BoxConstraints(maxWidth: 52, maxHeight: 46, minWidth: 52),
          prefixIcon: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 10),
            child: widget.prefixIcon != null
                ? SvgPicture.asset(
                    widget.prefixIcon!,
                  )
                : null,
          ),
          suffix: widget.suffixIcon,
          suffixIconConstraints:
              const BoxConstraints(maxWidth: 50, maxHeight: 34, minWidth: 45),
          suffixIcon: widget.isSecure
              ? Padding(
                  padding: const EdgeInsetsDirectional.only(end: 16),
                  child: IconButton(
                      constraints:
                          const BoxConstraints(maxWidth: 40, maxHeight: 40),
                      onPressed: () {
                        setState(() => passwordIsShwon = !passwordIsShwon);
                      },
                      icon: !passwordIsShwon
                          ? Assets.icons.closedEye.svg(width: 24, height: 24)
                          : Assets.icons.openEye.svg(width: 24, height: 24)),
                )
              : widget.icon != null
                  ? Padding(
                      padding: const EdgeInsetsDirectional.only(end: 16),
                      child: SvgPicture.asset(
                        widget.icon!,
                        colorFilter:
                            ColorFilter.mode(AppColors.grey, BlendMode.srcIn),
                      ),
                    )
                  : null,
          hintText: widget.title),
    );
  }
}
