import "package:flutter/material.dart";
import 'package:get/get.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Function? callBack;
  final double? height;
  const CustomAppBar(this.title, {this.callBack, this.height, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: const EdgeInsets.all(16),
        height: height ?? 100,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                  onTap: () => callBack != null ? callBack!() : Get.back(),
                  child: const Icon(Icons.arrow_back, size: 24)),
              const Spacer(),
              Text(title, style: Get.textTheme.bodyMedium)
            ]),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height ?? 100);
}
