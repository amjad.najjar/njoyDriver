import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

import '../gen/assets.gen.dart';
import '../styles/colors.dart';

class Loading extends StatelessWidget {
  const Loading({super.key});

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
      child: Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                boxShadow: [
                  BoxShadow(
                      color: Get.theme.shadowColor,
                      offset: const Offset(0, 3),
                      blurRadius: 6)
                ]),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Container(
                color: AppColors.white,
                padding: const EdgeInsets.all(32.0),
                child: Assets.animations.carAnimation
                    .lottie(width: Get.width * .3),
              ),
            ),
          ),
          const SizedBox(
            height: 24,
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Loading'.tr,
                style: Get.textTheme.displaySmall
                    ?.copyWith(color: Get.theme.colorScheme.secondary),
              ),
              const SizedBox(width: 16),
              const SpinKitWave(
                color: AppColors.black,
                size: 12,
              )
            ],
          )
        ],
      )),
    );
  }
}
