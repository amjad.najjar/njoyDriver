import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../styles/colors.dart';

class AppImage extends StatelessWidget {
  final String path;
  final ImageType type;
  final BoxFit fit;
  final Widget errorWidget;
  final Widget? loadingWidget;
  final BorderRadius borderRadius;
  final Border? border;
  final double? height;
  final double? width;
  final Color? backgroundColor;
  final EdgeInsets? margin;
  final List<BoxShadow>? shadow;
  const AppImage({
    required this.path,
    required this.type,
    this.fit = BoxFit.cover,
    this.errorWidget = const Icon(Icons.error_outline),
    this.borderRadius = BorderRadius.zero,
    this.loadingWidget,
    this.height,
    this.width,
    this.shadow,
    this.backgroundColor,
    this.margin,
    this.border,
  });
  @override
  Widget build(BuildContext context) {
    Widget defualtLoading = Shimmer.fromColors(
      baseColor: AppColors.grey.withOpacity(.25),
      highlightColor: AppColors.primary,
      child: Container(
        height: height,
        width: width,
        decoration:
            BoxDecoration(borderRadius: borderRadius, color: AppColors.grey),
      ),
    );
    return Container(
      height: height,
      width: width,
      margin: margin,
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: borderRadius,
        boxShadow: shadow != null ? shadow! : null,
        border: border,
      ),
      child: ClipRRect(
        borderRadius: borderRadius,
        child: Builder(
          builder: (context) {
            switch (type) {
              case ImageType.CachedNetwork:
                return CachedNetworkImage(
                  imageUrl: path,
                  fit: fit,
                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      loadingWidget ?? defualtLoading,
                  errorWidget: (context, _, i) => errorWidget,
                );
              case ImageType.Network:
                return Image.network(
                  path,
                  errorBuilder: (context, _, i) => errorWidget,
                  loadingBuilder: (BuildContext context, Widget child,
                      ImageChunkEvent? loadingProgress) {
                    if (loadingProgress == null) return child;
                    return loadingWidget ?? defualtLoading;
                  },
                  fit: fit,
                );
              case ImageType.Asset:
                return Image.asset(
                  path,
                  width: width,
                  height: height,
                  errorBuilder: (context, _, i) => errorWidget,
                  fit: fit,
                );
              case ImageType.File:
                return Image.file(
                  File(path),
                  fit: fit,
                  errorBuilder: (context, _, i) => errorWidget,
                );
              default:
                return const SizedBox();
            }
          },
        ),
      ),
    );
  }
}

enum ImageType { Network, File, CachedNetwork, Asset }
